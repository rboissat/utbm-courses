#include <stdio.h>
#include <stdlib.h>

extern int asm_sum(int,int);

void input_error(int errno);

int main(int arc, char* argv[]) {
  int a,b;

  if (arc != 3)
    input_error(1);

  a = atoi(argv[1]);
  b = atoi(argv[2]);

  printf("Affichage du résultat\n");

  printf("%d\n",asm_sum(a,b));

  return 0;
}

void input_error(int errno) {
  if (errno == 1) {
    fprintf(stderr,"Args missing\n");
    exit(EXIT_FAILURE);
  } else {
    fprintf(stderr,"UNKNOWN ERROR!\n");
    exit(EXIT_FAILURE);
  }
}
