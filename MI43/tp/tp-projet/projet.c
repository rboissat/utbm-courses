#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <cyg/kernel/kapi.h>
#include <cyg/hal/drv_api.h>
#include "usart.h"
#include "apmc55800.h"


#define RINGBUF_SIZE 32
cyg_interrupt int_obj;
cyg_handle_t toto;

/* pile et espace m�moire des thread*/
cyg_thread thread_s;	
char stack[1][4096];
cyg_thread_entry_t core;


/* structure de donn�es */
 typedef struct don {char* datas;
  unsigned int len;
} Sdonnees;

/* structure de donn�es pour le code d'erreur */
typedef enum {
  OK = 0,
  TIMEOUT = 2,
  OVERRUN = 3,
  FRAMING_ERROR = 4
} error_t;

/* structure de donn�es liste circulaire */
typedef struct ringbuf {
  char data[RINGBUF_SIZE];
  int start;
  int end;
  cyg_sem_t used;
  cyg_sem_t free;
  int overrun;
} ringbuf;

/* Initialisation de la liste circulaire */
void ringbuf_init(ringbuf* buf) {
  buf->start = 0;
  buf->end = 0;
  cyg_semaphore_init(&buf->used, 0);
  cyg_semaphore_init(&buf->free, RINGBUF_SIZE);
  buf->overrun = 0;
}

/* Fonction permettant d'ajouter un char dans la liste circulaire */
void ringbuf_add(ringbuf* buf, char c) {
  /* Si le ring buffer est bloqu� par un overrun */
  if (!cyg_semaphore_timed_wait(&buf->free, 1)) {
    buf->overrun = 1;
  } else {
  /* Sinon on ajoute le char � la fin de la liste */
    buf->data[buf->end] = c;
    buf->end = (buf->end + 1) % RINGBUF_SIZE;
    /* on lib�re le s�maphore used */
    cyg_semaphore_post(&buf->used);
  }
}

/* Fonction permettant de lire un char dans la liste circulaire */
error_t ringbuf_get(ringbuf *buf, int timeout, char *c) { 
  if (buf->overrun)
    return OVERRUN;

  if (!cyg_semaphore_timed_wait(&buf->used, timeout))
    return TIMEOUT;

  *c = buf->data[buf->start];
  buf->start++;
  /* on lib�re le s�maphore free */
  cyg_semaphore_post(&buf->free);

  return OK;
}

#define US1 ((StructUSART*)0xFFFC4000)

/* structure de donn�es d'�tat
 * Permet de controller l'�tat des donn�es
 * transmises et re�ues, les erreurs encontr�es, */
volatile struct {
  cyg_mutex_t write_mutex;
  int unlock_write;
  ringbuf recv_buffer;
  ringbuf read_buf;
  char tmp;
  int has_newchar;
  int overrun;
  int frame_err;
} state;

/* Initialisation de l'�tat */
void init_state() {
  cyg_mutex_init(&state.write_mutex);
  state.unlock_write = 0;
  ringbuf_init(&state.read_buf);
  state.has_newchar = 0;
  state.overrun = 0;
  state.frame_err = 0;
}

/* ISR: */
static cyg_uint32 usart_isr(cyg_vector_t vector, cyg_addrword_t data) {
  /* Si on est en fin de transmission */
  if (US1->US_CSR & US_ENDTX) {
    /* on d�sactive l'IRQ US_ENDTX */
    US1->US_IDR = US_ENDTX;
    /* on signale que le s�maphore d'�criture doit �tre d�bloqu� */
    state.unlock_write = 1;
  }
  /* Si on pr�t � transmettre */
  if (US1->US_CSR & US_RXRDY) {
    /* On stocke temporairement le contenu du Receiver Hosting Register */
    state.tmp = US1->US_RHR;
    /* On met � jour l'�tat */
    state.has_newchar = 1;
  }
  /* on acquitte l'interruption */
  cyg_interrupt_acknowledge(3);

  /* On retourne en appellant le DSR */
  return (CYG_ISR_HANDLED | CYG_ISR_CALL_DSR);
}

/* DSR: */
static void usart_dsr(cyg_vector_t vector, cyg_ucount32 count, cyg_addrword_t data) {
  /* Si l'�tat indique que l'�criture est bloqu�e, on la d�bloque */
  if (state.unlock_write) {
    cyg_mutex_unlock(&state.write_mutex);
    state.unlock_write = 0;
  }
  /*Si l'�tat indique un nouveau caractere, on le lit */
  if (state.has_newchar) {
    ringbuf_add(&state.recv_buffer, state.tmp);
    state.has_newchar = 0;
  }
}

/* Initialisation USART */
static void init_usart() {
  /* Configuration de l'holorge */
  StructAPMC* apmc=APMC_BASE;
  apmc->APMC_PCER=(1<<3);

  /* USART gere les interruptions, on desactive la gestion des ports s�rie par le PIOA */
  StructPIO * PIOA;
  PIOA->PIO_PDR = PA18 | PA 19;

  /* Reset du receiver et transmitter */
  US1->US_CR = US_RSTRX | US_RSTTX;

  /* Mode de fonctionnement de l'USART:
   * local loopback
   * char de 8 bits
   * horloge interne */
  US1->US_MR = US_CHMODE_LOCAL_LOOPBACK | US_CHRL_8 | US_CLKS_MCK;

  /* R�glage du Baud Rate � 38400bx */
  US1->US_BRGR = 52; 

  /* Reset de la d�sactivation des Interruptions */
  US1->US_IDR = ~0;

  /* Activation de l'interruption RXRDY */
  US1->US_IER = US_RXRDY;

  /* Activation de la transmission et de la r�ception */
  US1->US_CR = US_TXEN | US_RXEN;
}

void write(char* data, unsigned int len) {
  cyg_mutex_lock(&state.write_mutex);

  /* On attend que le port soit pr�t */
  while (!(US1->US_CSR & US_TXEMPTY));

  US1->US_TPR = data;
  US1->US_TCR = len;
  US1->US_IER = US_ENDTX;
}

int read(char* data, unsigned int len) {
  while (len) {
    error_t result = ringbuf_get(&state.recv_buffer, 100, data); 
     printf("%s", result);

    data++;
    len--;
  }

  return 42;
}

void core(cyg_addrword_t arg){
  char* data="0123456789\0";
  unsigned int len = strlen(data);
  char* recu="";
  unsigned int len2;
  write(data,len);
  read(recu,len);
  printf("%s",recu);
}

void cyg_user_start(void) {
  /* On initialise l'�tat du programme */
  init_state();

  /* On initialise l'USART */
  init_usart();

  /* On d�bloque les ISR via eCOS */
  cyg_drv_isr_unlock();

  /* Cr�ation de l'objet interrupt */
  cyg_interrupt_create(
      3,
      4,
      &state,
      usart_isr,
      usart_dsr,
      &toto,
      &int_obj);

  /* On attache l'interruption */
  cyg_interrupt_attach(toto);

  /* On autorise le vector d'interruption 3 */
  cyg_interrupt_unmask(3);

  Sdonnees arg;

  cyg_thread_create( 4,core,(cyg_addrword_t) &arg," thread ecriture",
      (void *)stack[0], 4096,&core, &thread_s);	
}


