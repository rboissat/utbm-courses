#ifndef CYGONCE_PKGCONF_HAL_ARM_AT91_H
#define CYGONCE_PKGCONF_HAL_ARM_AT91_H
/*
 * File <pkgconf/hal_arm_at91.h>
 *
 * This file is generated automatically by the configuration
 * system. It should not be edited. Any changes to this file
 * may be overwritten.
 */

#define CYGHWR_HAL_ARM_AT91 M55800A
#define CYGHWR_HAL_ARM_AT91_M55800A

#endif
