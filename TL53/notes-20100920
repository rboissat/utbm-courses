EX: On considère une exp A qui consiste à extraire au hasard une carte
d'un jeu de 32 cartes.

Calculer l'entropie de A.

extraction équiprobable: pk = 1/32 qqst 1 <= k <= 32

H(A)  = - 32 * (1/32. * log(1/32.,2))
      = 5 bits

Interprétation:

Pour savoir quelle carte a été extraite, il nous faut :
* un bit pour la couleur
* un bit pour cœur ou carreau, respectivement trèfle ou pique
* un bit pour savoir savoir si la carte appartient à (7,8,9,10) ou (V,D,R,A)
* un bit pour savoir savoir si la carte appartient à (7,8) ou( 9,10), resp (V,D)
  ou (R,A)
* un bit pour savoir si la carte a été tirée.

____________________________________________________________________________
PROPRIÉTÉS:

* H(A) => 0

* Si pour un certain évènement i nous avons p(i) = 1 alors H(A) = 

* Pour toute répartition p1, p2, ..., pn nous avons 
  H(p1, p2, ..., pn) <= H(1/n, 1/n, ..., 1/n) = log2(n)

==> L'entropie est maximale pour des évènements équiprobables.

____________________________________________________________________________
____________________________________________________________________________
ENTROPIE LIÉE À UN COUPLE DE VARIABLES

Soient deux expériences A et B.

A = a1 -> p1, a2 -> p2, ... an -> pn

B = b1 -> q1, n2 -> q2, ... bm -> qm


L'évènement qui consiste en la réalisation conjointe des évènements ak et bl
sera noté (ak; bl) et sa probabilité par p(ak; bl) avec 1 <= k <= n
et 1 <= l <= m.

On a alors :

#
#  p(ak; bl) = pk * p(bl/ak) = ql * p(ak/bl)
#

* Premier cas : A et B sont indépendants, dans ce cas p(ak/bl) = ql
  Donc p(ak ; bl) = pk * ql
#
#  Ainsi H(A ; B)  = H(A) + H(B)
#

* Deuxième cas : A et B sont dépendants, dans ce cas pour chaque évènement bl
  nous avons n possibilités : q(1l), q(2l), ..., q(nl)
  avec q(kl) = p(bl/ak)  1 <= l <= m et 1 <= k <= n

  q(kl) représente la probabilité de réalisation de l'évènement bl en supposant
  que l'expérience A nous ait fourni ak.
#
#  Ainsi on a Hk(B) = - Σ(l=1->m) q(kl) * log2(q(kl))
#

Hk(B) est l'entropie conditionnelle de B quand l'évènement ak est donné.

DÉFINITION:
#
#  HA(B) = Σ(k=1->n) pk * Hk(B)   est l'entropie conditionnelle de B
#                                 quand A est donnée.
#

Remarque :  Si A et B sont indépendantes alors dans ce cas q(kl) = ql
            d'où Hk(B)  = H(B)

            donc HA(B)  = Σ(k=1->n) pk * H(B)
                        = H(B) * Σ(k=1->n) pk
                        = H(B) * 1
                        = H(B)

PROPRIÉTÉ:  Si A et B sont dépendantes on a :
#
#  H(A; B) = H(A) + HA(B) = H(B) + HB(A)
#

et 

#
#  HA(B) <= H(B)
#

et

#
# I(A,B) = H(B) - HA(B) = H(A) - HB(A) = I(B,A)
#

C'est L'INFORMATION APPORTÉE PAR A SUR B OU B SUR A

Remarque :  Si A et B sont indépendantes alors HA(B) = H(B)
            et dans ce cas I(A,B) = 0

PROPRIÉTÉS:
  * H(A,B) <= H(A) + H(B)   égalité possible que si A et B sont indépendantes.
  * HA(B) = HB(A) + H(B) - H(A)

Si A détermine complètement B, dans ce cas HA(B) = 0 et I(A,B) = H(B).

