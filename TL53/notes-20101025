CHAPITRE 3 : CANAUX DE TRANSMISSION

On définit un canal de transmission par :
  * Un alphabet d'entrée  X = {x1, x2, ... , xn}

  * Un alphabet de sortie Y = {y1, y2, ... , ym}

  * Une matrice de transition dont les éléments Tij sont les probabilités
    de transition de X à Y.
    C'est à dire Tij = p(Y=yi/X=xj) la proba conditionnelle de recevoir
    yi lorsque xj est émis.

1. Bruit et capacité de canal
-----------------------------

Déf:
  HY(X) représente la quantité moyenne d'information qui pendant la
  transmission se perd dans la voie à cause du bruit.

Si de la source X on transmet la quantité d'information H(X),
il s'ensuit qu'à l'utilisateur arrivera seulement une quantité d'info
égale à :
#
#  I(X,Y) = H(X) - HY(X)
#

Si on considère la valeur max de I(X,Y) pour toutes les probabilités d'entrée
possibles on met ainsi en évidence la quantité max d'info qui peut être
acheminée dans le canal.

D'où :

Déf:
  On appelle Capacité d'un canal de transmission la valeur :
#
#  C = max( I(X,Y) )
#

Exemples de calcul de capacité
------------------------------
  * Canal parfait
       1
  X --------> Y
  A    "      A'
  B    "      B'
  C    "      C'
  D    "      D'

Résolution:
  On calcule I(X,Y) = H(X) - HY(X) car la connaissance de Y implique celle de X !
  Donc HY(X) = 0

  D'où C  = max( I(X,Y) )
          = max( H(X) )       (quand les probas sont équiprobables)
          = log2(4)
          = 2 bits


  * Canal imparfait
  X      1     Y
  A, B ----->  A'
  C, D ----->  B'

Résolution:
  HX(Y) = 0 car la connaissance de X implique celle de Y.
  Ainsi I(X,Y) = H(Y)

  Calcul de p(A') et p(B'):
    * p(A') = p(Y=A'/X=A) * p(A) + p(Y=A'/X=B) * p(B)
            = p(A) + p(B)

    * p(B') = 1 - (p(A) + p(B))

  Posons p = p(A) + p(B)

  Il vient alors :
    I(X, Y) = H(Y)
            = - p(A') * log2(p(A')) - p(B') * log2(p(B'))
            = - p * log2(p) - (1-p) * log2(1-p)
            = H2(p)

  H2(p) entropie d'une source à deux symboles dont l'un est de proba p
  (et l'autre 1-p).


  H2'(p) = log((1-p) / p)

  H2'(p)  =>  0
          =>  log 1

  Donc (1-p) / p => 1

  Donc    p <= 1/2

  Tableau de variation :

  p     | 0       1/2       1
  ---------------------------
  H2'(p)|     +    0    -
  ---------------------------
  H2(p) |     ^ ^  1 ^ ^
        |0 ^ ^           ^ ^ 0

  C = max( I(X,Y) )
    = 1   quand p = p(A) + p(B) = 1/2


2. Canal symétrique
--------------------

Déf:
  Un canal est dit symétrique s'il existe une partition de l'alphabet de sortie
  telle que pour tout élément de cette partition la sous-matrice de transition
  vérifie :
    * Toutes les lignes sont identiques à des permutations près
    * "       "  colonnes "   "         " "   "             "

Théorème:
#
#  Pour un canal symétrique, la capacité est atteinte pour une loi uniforme sur l'entrée.
#

Ex:
  X           Y
  0 -- 1-p -> 0
   \_    ___/
    p\__/ p
      _/\__
  1 _/_____\> 1
       1-p

  p = la proba pour qu'un symbole soit changé = erreur

  La matrice de transition est :

            Y
  T = ( 1-p   p   )
      (           )   X
      ( p     1-p )

  Calcul de I(X,Y) = H(Y) - HX(Y) :

    Calcul de p(Y=0) :
      p(Y=0)  = p(Y=0/X=0) * p(0) + p(Y=0/X=1) * p(1)
              = (1-p) * p(0) + p * p(1)
              = 1/2 - p/2 + p/2
              = 1/2

    Donc p(Y=1) = 1/2

    Donc H(Y) = log2(2)
              = 1 bit

  Pour le calcul de HX(Y) il faut évaluer H(Y/X=0) et H(Y/X=1)
  En effet
    H(Y/X=0) = -p log2(p) - (1-p) log2(1-p) = H2(p)

  D'où
    HX(Y) = p(X=0) H(Y/X=0) + p(X=1) H(Y/X=1)
          = 1/2 H2(p) + 1/2 H2(p)
          = H2(p)

  Finalement
    C(p) = 1 - H2(p)


  Interprétation:
    Si p = 1/2  on a  C = 0   ==> Canal inutile.
    Si p > 1/2  on a  C > 0   ==> On part de 0 ou de 1 pour arriver à 1 ou 0
                                  On a donc intérêt à échanger les lettres
                                  de l'alpha. de sortie.

    Si p = 1|0  on a  C = 1   ==> Canal parfait.


Théorème:
  On considère une source S d'entropie H∞(S) délivrant ses symboles avec un
  débit Ds et un canal de capacité C utilisé au débit Dc.

  Si le débit d'entropie H'(S) = Ds * H∞(S) < C' = C * Dc
  Alors qqst ϶, il existe un code pour transmettre le contenu de la source
  sur le canal tel que :
#
#   p(erreur après décodage) < ϶
#

Interprétation: 

On peut transmettre le contenu de la source sur le canal avec une proba d'erreur
aussi petite que souhaitée.

Ex:
  Soit une source binaire sans mémoire S telle que
    p(S=0) = 0.98   p(S=1) = 0.02
    Ds = 600 kbits/s

    On dipose d'un canal binaire symétrique de proba d'erreur p = 10¯³ avec
    Dc = 450 kbits/s

    Peut-on transmettre sur le canal ?

    H∞(S) = -0.98 * log2(0.98) - 0.02 * log2(0.02)
          = 0.1414 bit

    H'(S) = Ds * H∞(S)
          = 84846 bit/s

    Canal binaire symétrique ===> C(p) = 1 - H2(p) pour p = 10¯³

    C(10¯³) = 1 - H2(10¯³)
            = 0.9886 bit

    Donc
      C'  = 0.9886 * 450 * 10³
          = 444870 bit/s

    Finalement
      H'(S) < C'    On peut donc transmettre sur ce canal.

