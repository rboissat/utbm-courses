% Les hommes
homme(odillon).
homme(roger).
homme(edouard).
homme(marius).
homme(vincent).
homme(marcel).
homme(blaise).
homme(arnaud).

% Les femmes
femme(genevieve).
femme(pauline).
femme(tatiana).
femme(melanie).
femme(roseline).
femme(paule).
femme(agnes).
femme(prisca).
femme(martine).

% les enfants
enfant(melanie,genevieve).
enfant(melanie,odillon).

enfant(pauline,genevieve).
enfant(pauline,odillon).

enfant(edouard,genevieve).
enfant(edouard,odillon).

enfant(roseline,melanie).
enfant(roseline,roger).

enfant(marcel,roger).
enfant(marcel,melanie).

enfant(marius,edouard).
enfant(marius,tatiana).

enfant(prisca,edouard).
enfant(prisca,tatiana).

enfant(agnes,vincent).
enfant(agnes,roseline).

enfant(arnaud,vincent).
enfant(arnaud,roseline).

enfant(martine,marcel).
enfant(martine,paule).

enfant(blaise,marcel).
enfant(blaise,paule).

% Generique
pere(X,Y) :- homme(X), enfant(Y,X).
mere(X,Y) :- femme(X), enfant(Y,X).
soeur(X,Y) :- femme(X), pere(Z,X), pere(Z,Y).
frere(X,Y) :- homme(X), pere(Z,X), pere(Z,Y).
frere_ou_soeur(X,Y) :- frere(X,Y); soeur(X,Y).
oncle(X,Y) :- frere(X,(pere(Z,Y);mere(Z,Y))).
tante(X,Y) :- soeur(X,(pere(Z,Y);mere(Z,Y))).
grand_parent(X,Y) :- enfant(X,(pere(Z,Y);mere(Z,Y))).
cousin_cousine(X,Y) :- enfant(X,(oncle(Z,Y);tante(Y,Z))).
