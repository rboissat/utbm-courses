%%% EXO 1
% 1)
member(L,[[a,b],[c,d,e]]), member(X,L).
% renvoie les éléments des sous-listes

member(L,[[a,b],[c,d,e]]), member(X,L),!.
% renvoie L = [a,b], X = a  (premier élément de la requete totale)

member(L,[[a,b],[c,d,e]]),!,member(X,L).
% renvoie L =[a,b], X =a et L = [a,b] et X = b
% traite tout le premier element du premier prédicat, et tout le deuxième

!,member(L,[[a,b],[c,d,e]]), member(X,L).
% comme le premier prédicat, renvoie tout

% 2)
% pour obtenir la première solution d'un prédicat à l'aide d'une coupure, il
% suffit de couper après le prédicat

% 3)
si-alors-sinon(C,A,S) :- C,!,A.
si-alors-sinon(C,A,S) :- S.

% 4)
% not
