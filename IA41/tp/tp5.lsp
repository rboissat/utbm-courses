(defun fn1(x)
  (if ( >= x 0)
    ( + ( * 5 x x ) ( * 20 x ) -5 )
    ( * ( - 50 ( * 3 x ) ) 12 )
  )
)

(defun puissance(n p)
  (if (= p 1) n
    ( * n (puissance n (- p 1) ))
  )
)

(defun fibonacci(n)
  (if ( or (= n 0) (= n 1) ) 1
    ( + (fibonacci( - n 1 )) (fibonacci( - n 2 )) )
  )
)

(defun liste_atomes(l)
  ( or ( null l ) ( and (atom ( car l )) (liste_atomes (cdr l)) ) )
)

(defun longueur(l)
  (if (null l) 0
    (+ 1 (longueur (cdr l))))
)
