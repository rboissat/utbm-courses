#!/bin/bash -x
# Interfaces
EXT="eth0"
INT="eth1"

# IP networks and hosts : on autorise la rangée 2
EXT_IP="192.168.1.1"
ALLOWED="192.168.1.2"
HTTP_SERV="192.168.11.2"

## Ip_forward
echo "1" > /proc/sys/net/ipv4/ip_forward

## Flush tables
iptables -F
iptables -X
iptables -t nat -F
iptables -t nat -X
iptables -t mangle -F

## Default policies
iptables -P INPUT DROP
iptables -P OUTPUT ACCEPT
iptables -P FORWARD DROP
iptables -t nat -P PREROUTING ACCEPT
iptables -t nat -P POSTROUTING ACCEPT
iptables -t nat -P OUTPUT ACCEPT

### Loop
iptables -A INPUT -i lo -j ACCEPT

# on interdit le ICMP Echo-Reply
iptables -A INPUT -p icmp --icmp-type ping -j DROP
iptables -A FORWARD -p icmp --icmp-type ping -j DROP

# on autorise le fw de port depuis l'IP autorisée
iptables -A FORWARD -o $INT -m state --state NEW -j ACCEPT
iptables -A FORWARD -o $INT -m state --state ESTABLISHED,RELATED -J ACCEPT
iptables -t nat -A PREROUTING -s $ALLOWED -d $EXT_IP -p tcp --dport 80 -j DNAT --to $HTTP_SERV:80
