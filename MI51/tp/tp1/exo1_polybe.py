#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" Proof of concept of Polybe's square cipher
"""
import sys

# alphabet w/o 'w'
al = "abcdefghijklmnopqrstuvxyz"

def print_help():
    print "Usage: ./exo1_polybe.py [key] [string]"

def gen_table(table,key):
    for c in key:
        table = table.replace(c,'')
    table = key+table
    return table

def po_cipher(clrtxt):
    i_col = 0
    i_lin = 0
    lst = []
    for c in clrtxt:
        index = al.index(c)+1
        div = divmod(index, 5)
        i_lin = div[0]  if index%5 == 0 else div[0]+1
        i_col = 5       if index%5 == 0 else div[1]
        lst.append((i_lin,i_col))

    return lst

def po_decipher(enctxt):
    pass

if __name__ == "__main__":
    if len(sys.argv) > 2:
        key = sys.argv[1]
        str = sys.argv[2]
        al = gen_table(al,key)
        print "New table: %s\n" % al
        print po_cipher(str)
        print 
    else:
        print_help()

