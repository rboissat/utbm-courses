#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" Chiffre affine
"""
import sys
import random

al = " abcdefghijklmnopqrstuvwxyz0123456789_"

def print_help():
    print "Usage: ./exo1_chiffre_affine [int a] [int b] [cleartext]"
    print "Reminder: y = (a.x + bi) mod card(alphabet)"

def extended_gcd(a, b):
    # We use Bézout identity as defined by ax + by = PGCD(a,b)
    # Here we assume to get b=0 and y=0 by the end
    x, last_x = 0, 1
    y, last_y = 1, 0
    while b:
        quotient = a // b
        a, b = b, a % b
        x, last_x = last_x - quotient*x, x
        y, last_y = last_y - quotient*y, y
    # We return the  and the the last non-null remainder (pcgd)
    return (last_x, a)

def inverse_mod(a, m):
    # We expect to get gcd=1 from this.
    # This way, we would have the following:
    # a*x + b*y = 1 and  x the modular inverse of a mod b.
    x, gcd = extended_gcd(a, m)
    if gcd == 1:
        # x is the inverse, but we want to be sure a positive number is returned
        return (x + m) % m
    else:
        # if gcd!=1 then a and m are not coprime and the inverse does not exist.
        return None

def af_cipher(clrtxt,a,b):
    return "".join([al[(a * al.index(x) + b) % len(al)] for x in clrtxt])

def af_decipher(enctxt,inv,b):
    return "".join([al[(inv * (al.index(x) - b)) % len(al)] for x in enctxt])

if __name__ == "__main__":
    if len(sys.argv) > 3:
        a = int(sys.argv[1])
        b = int(sys.argv[2])
        str = sys.argv[3].lower()
        inv = inverse_mod(a,len(al))
        encstr = af_cipher(str,a,b)
        # we exit if a and m = len(al)+1 are not coprime
        if inv == None:
            print "a must be coprime to %d" % len(al)
            sys.exit(42)
        print "Cleartext string: \"%s\" for a=%d and b=%d" % (str,a,b)
        print "Modular Inverse: %d" % inv
        print "Encrypted string: \"%s\"" % encstr
        print "Decrypted string: \"%s\"" % af_decipher(encstr,inv,b)

    else:
        print_help()

