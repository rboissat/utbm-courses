#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" Proof of concept of the Caesar cipher based on shifted substitutions
"""
import sys
import random

al = " abcdefghijklmnopqrstuvwxyz0123456789_"

def ca_cipher(clrtxt,shift):
    ''' Return an enciphered string '''
    return "".join([al[(al.index(x) + shift) % len(al)] for x in clrtxt])

def ca_decipher(enctxt,shift):
    ''' Return an deciphered string '''
    return "".join([al[(al.index(x) - shift) % len(al)] for x in enctxt])

def ca_decrypt(enctxt):
    ''' Attempt to brute-force an encrypted string compared to user input '''
    txt = ""
    print "Beginning decrypt by random shift:\n"
    while txt != str:
        i = random.randint(1, len(al))
        txt = ca_decipher(enctxt, i)
        print "s=%02d %s" % (i, txt)
    print"\nShift is: %d\n" % i

def print_help():
    print "Usage: ./exo1_caesar.py [shift] [cleartext]"

if __name__ == "__main__":
    if len(sys.argv) > 2:
        sft = int(sys.argv[1])
        str = sys.argv[2].lower()
        enc_str = ca_cipher(str,sft)
        dec_str = ca_decipher(enc_str,sft)
        print "\nShift from argv[1] : %d"       % sft
        print "Effective shift mod(%d) : %d"    % (len(al), sft%len(al))
        print "Cleartext string: %s"            % str
        print "Encrypted string: %s"            % enc_str
        print "Decrypted string: %s\n"          % (dec_str)
        if len(sys.argv) > 3 and sys.argv[3] == "--brute-force":
            ca_decrypt(enc_str)
    else:
        print_help()

