CRYPTANALYSE
____________________________________________________________________________
Technique pour "cracker" les clés privées.

Exemple : Chiffre affine simplifié

E(x,a) = a*x
D(y,a) = 1/a * y

MESSAGESECRET ==> ACSII : M'=77 69 83 83....

a = 3

E(x,3) = C = 231 207 249 249 195 213 207 249 207 201 246 207 252

S  = {P, P, Ke, Kd, E, D}
 k

CRYPTANALYSE : Soit dévoiler la clé de déchiffrement, soit directement le
texte en clair.

Attaques :
  - Recherche exhaustive
  - Diviseur commun
  - Analyse fréquentielle
  - Déduction
____________________________________________________________________________
RECHERCHE EXHAUSTIVE "Brute force"
  - essai-erreur sur l'ensemble des clés de déchiffrement possible
    jusqu'à l'obtention d'un texte cohérent.

____________________________________________________________________________
DIVISEUR COMMUN
  - la clé est donnée par 1 / pgcd(c) ==> 1/3

____________________________________________________________________________
DÉDUCTION
  - 65 / min(c) = 65 / 195 = 1/3

____________________________________________________________________________
ANALYSE FRÉQUENTIELLE
  - probabilité qu'une valeur chiffrée corresponde à une valeur en clair

On suppose que la langue de rédaction est le français. Il faut suffisamment
de texte chiffré pour réaliser cette attaque.

f(207) > f(249 > f(195)

Analyse de la fréquence d'apparition des caractères dans la texte chiffré.
Le cryptosystème ne doit pas masquer les schémas linguistiques.

Indice de coïncidence (http://fr.wikipedia.org/wiki/Indice_de_coïncidence):
  Désigne la probabilité que deux caractères de l'alphabet pris au hasard
  dans le texte chiffré sont identiques pour une langue donnée.

en  |   0.0667
fr  |   0.0778

____________________________________________________________________________
MESURE DE SÉCURITÉ DES CRYPTOSYSTÈMES

- Cassage complet : Découverte de la clé de déchiffrement
- Obtention globale : Découverte d'un cryptosystème équivalent
- Obtention locale : Décryptage d'un message chiffré isolé
- Obtention d'informations : infos sur la composition de la clé ou fragment
                             de texte en clair.

Complexité :
  * en information = quantité nécéssaire pour l'exécution de l'algo
  * en temps = temps nécessaire à l'achèvement de l'attaque
  * en espace = espace mémoire et disque nécessaire

Classification des attaques :
  1 à texte chiffré connu
  2 à texte en clair connu
  3 à texte en clair choisi
  4 ................ adaptatif

1. Découvrir à l'aide d'un même algo le plus grand nombre de textes en clair
   et/ou découvrir la ou les clés de déchiffrement à employer.

2. Découvrir la ou les clés de (dé)chiffrement employées

3. Les textes en clair sont choisis par le cryptanalyste

4. Le cryptanalyste choisit les textes en clair en fonction des textes chiffrés
   précédents.

____________________________________________________________________________
COÛTS
----------------------------------------------
|coût    |  longueur de clé (bits)           |
|--------------------------------------------|
|        | 40  | 56  | 64  | 80 | 112  | 128 |
|--------------------------------------------|
|100 k$  | 2s  | 35h | 1y | 700c |10¹⁴y|10¹⁹y|
|--------------------------------------------|
|100 G$  | 2us | 0.1s| 32s| 24d  |10⁸y | 10¹³|
----------------------------------------------

____________________________________________________________________________
CRYPTANALYSE DES SYSTÈMES À SUBSTITUTION
  * Recherche exhaustive --> substitutions simples _uniquement_

____________________________________________________________________________
CRYPTANALYSE DES SYSTÈMES À TRANSPOSITION
  * n! possibilités pour un texte de longueur n

  MESSAGESECRET -> 13! > 6.2 * 10⁹ clés possibles

____________________________________________________________________________
ATTAQUE PAR DICO
  * surtout employé pour trouver des mots de passe.

____________________________________________________________________________
CRYPTANALYSE LINÉAIRE
  * mise en œuvre pour casser DES

____________________________________________________________________________
CRYPTANALYSE DIFFÉRENTIELLE

