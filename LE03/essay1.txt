"Are you someone who enjoys the unfamiliar and who easily adapts to new
surroundings, or do you find such situations unpleasant? Give at least one
detailed example from your own experience to justify your answer."
____________________________________________________________________________

Usually, undergraduate students live their first experience of living outside
the family sphere. For most of them, it can take some time to handle this
important step in life, but still, they manage to live on their own.

My personal experience is somewhat different. Indeed, I signed up for a military
boarding school since my first year of high school. I spent four years far away
from my family, with rare and short vacations. Obviously, I ought to learn to
live on my own in a small society composed of other boys and girls, immersed in
a military structure. I took me time, energy and sometimes tears, but I went
through and strongly believe that I couldn't be the person I am today without
this eye-opening experience.

Besides, I would like to highlight the point of learning life the hard way.
Indeed, I am rather sure that someone who endure some difficulties in life and
who know sometimes what it takes to succeed by failling to a task would be a
better person, acute and mature, able to take a distance look when facing a
problem. On the contrary, someone who born with a silver spoon, who has always
lived in a protective sphere like an opaque and smooth bubble, without any
sight on how the 'real' world is, then this person would be somewhat lost and
pessimist as soon as a problem happens.

To conclude and sum up my point of view, I am not advocating the Spartian way of
life, but just saying that everyone should be able to handle their lives by
themselves, and to my humble opinion, adapting to a new environment consists
only in a small part.

