/***********************************************************************
 * Subject: TP2 - Exo1
 * Author: Romain BOISSAT
 * Date: 13/10/2008
 *
***********************************************************************/

# include <stdio.h>
# include <stdlib.h>

// Prototypes
float integraleInverse(float BI, float BS, int NI);

int main(int argc, char *argv[])
{
  int NbInt=0; // Nombre d'intervalles
  int result=0; // Resultat
  float BorneSup=0; // Borne supérieure
  float BorneInf=0; // Borne inférieure

  // On vérifie la bonne utilisation du programme 
  if(argc != 4)
  {
    printf("Usage: exo1 [BORNE_INF] [BORNE_SUP] [NB_INTERVALLES]\n");
    exit(1);
  }

  // On récupère les arguments
  BorneInf = atof(argv[1]);
  BorneSup = atof(argv[2]);
  NbInt = atoi(argv[3]);

  // Appel de la fonction
  integraleInverse(BorneInf,BorneSup,NbInt);

  return 0;
}

float integraleInverse(float BI, float BS, int NI)
{
  int i=0;
  float h=0; // Pas de la méthode.
  float x=0; // Position du trapèze courant
  float y=0; // position du trapèze "suivant"
  float A=0; // approximation de l'intégrale
  float T=0; // aire d'un trapèze

  // On détermine la largeur du pas
  h = (BS - BI) / NI;

  // On effectue le calcul de l'aire par la méthode des trapèzes
  x=BI;
  for(i;i<NI;i++)
  {
    y = x + h;
    T = ((1.0/x + 1.0/y)) * (h/2.0);
    A = A + T;
    x = y;
  }

  // Affichage des paramètres et du résultat
  printf("L'integrale de la fonction 1/x entre %f et %f avec %d intervalles \
est %f \n", BI, BS, NI, A);

  // Renvoi de la valeur d'execution
  return 0;
}
