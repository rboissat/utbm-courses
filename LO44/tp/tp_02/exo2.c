/*---------------------------------------------------
 * TP2 
 * Romain BOISSAT
 * Exercice 2
 * ---------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// Définition de la structure monome
typedef struct monome {
  float coef;
  int degre;
} Monome;

int main(int argc, char * argv[])
{
  float x=0;
  double var=0;
  double result=0;
  int nbMonome;
  int coefMonome;
  int i;

  printf("Nombre de monomes ?\n");
  scanf("%d", &nbMonome);
  if(nbMonome <= 0)
  {
    printf("le nombre de monomes doit être supérieur à 0\n");
    return EXIT_FAILURE;
  }

  // déclaration du polynome
  Monome polynome[nbMonome];

  // Saisie des coefficients
  for(i=nbMonome-1; i >= 0; i--)
  {
    printf("Saissez le coefficient du monome de degré %d\n", (i));
    scanf("%d", &coefMonome);
    polynome[i].coef = coefMonome;
  }

  // Saisie de x
  printf("Valeur de x ?\n");
  scanf("%f", &x);

  // Calcul de P(x)
  for(i=0; i < nbMonome; i++)
  {
    var = pow(x,i)*polynome[i].coef;
    result = result + var;
  }

  // Affichage du résultat
  printf("\nP(x) = %lf \n",result);

  return 0;
}

