/*-------------------------------------------------
 * TP1
 * Romain BOISSAT
 * Exercice 3
 *------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>

typedef struct produit {
  int code;
  int quantite;
  float prix;
} produit;

// prototypes
void affiche_produit(struct produit p);

int main(void)
{
  // Déclaration du tableau
  produit p[4];
  int saisie=0;
  int i=0;

  p[0].code = 3;
  p[0].quantite = 0;
  p[0].prix = 5.80;

  p[1].code = 5;
  p[1].quantite = 1;
  p[1].prix = 12.40;

  p[2].code = 7;
  p[2].quantite = 2;
  p[2].prix = 13.50;

  p[3].code = 6;
  p[3].quantite = 10;
  p[3].prix = 10.0;

  // Demande de seuil
  printf("Seuil?\n");
  scanf("%d",&saisie);

  // Traitement de la requete
  for(i=0; i < 4; i++)
  {
    if(p[i].quantite < saisie)
      affiche_produit(p[i]);
  }

  return 0;
}

void affiche_produit(struct produit p)
{
  printf("Produit demandé: code %d, quantité %d, prix %f", p.code, p.quantite,
      p.prix);
  printf("\n-------------------\n");
}
