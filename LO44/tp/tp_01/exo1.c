/*---------------------------------------------------------------
 * TP 1
 * Romain BOISSAT
 * Exercice 1
 * ----------------------------------------------------------------*/
/*****************************************
* hello_world.c                          *
* Auteur : Meignan D.                    *
* Date : 14/09/2007                      *
* Version : v1.0                         *
*                                        *
* Ce programme affiche "Hello world!"    *
* sur la sortie standard.                *
*****************************************/

// Librairie standard pour les entrées/sorties
#include <stdio.h>
// Librairie pour les types et constantes prédéfinies
#include <stdlib.h>


/*****************************************
* Fonction main affichant                *
* "Hello world!".                        *
*                                        *
* Paramètres : aucuns                    *
* Retour : int, EXIT_SUCCESS             *
*****************************************/
int main(void)
{
	printf("Hello world!\n");
	return EXIT_SUCCESS;
}
