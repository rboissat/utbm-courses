
/************************************************************
 *                                                          *
 * Exercice no 2 - TP no1 - LO44                            *
 *                                                          *
 * Auteur : David Meignan                                   *
 *                                                          *
 * Le programme suivant effectue la multiplication d'une    *
 * matrice par un vecteur.                                  *
 ************************************************************/

// Inclusion de la librairie pour les entrees/sorties
#include <stdio.h>
// Librairie pour les types et constantes prédéfinies
#include <stdlib.h>


// Taille de la matrice
#define L 2
#define N 3

// Prototype des fonctions
void affiche_vecteur(int nb_elements, float v[nb_elements]);
void affiche_matrice(int nb_lignes, int nb_colonnes, float m[nb_lignes][nb_colonnes]);

int main(void) {
	
	// Déclaration explicite d'une matrice A
	float A[L][N] = {
			{1.0 , 0.0 , 2.0} ,
			{-1.0 , 3.0 , 1.0}
		};
	
	// Déclaration explicite d'un vecteur B
	float B[N] = {3.0 , 1.0 , 2.0};
	
	// Déclaration du vecteur resultat
	float R[L];

	// Calcul de la multiplication de la matrice A par le vecteur B

	//--------------------------------------
	//	INSERER ICI VOTRE CODE...
	//--------------------------------------


	// Affichage du résultat
	printf ("\n Matrice A : \n");
	affiche_matrice(L, N, A);
	printf ("\n Vecteur B : \n");
	affiche_vecteur(N, B);
	printf ("\n Vecteur R = A*B : \n");
	affiche_vecteur(L, R);
	
	// Retour du code d'execution avec succes
	return EXIT_SUCCESS;
}

/************************************************************
 *                                                          *
 * La fonction suivante réalise l'affichage d'un vecteur    *
 * sur la sortie standard.                                  *
 *                                                          *
 * nb_elements : Le nombre d'elements du vecteur.           *
 * v : Le vecteur a afficher.                               *
 ************************************************************/
void affiche_vecteur(int nb_elements, float v[nb_elements]) {
	
	//--------------------------------------
	//	INSERER ICI VOTRE CODE...
	//--------------------------------------

}

/************************************************************
 *                                                          *
 * La fonction suivante réalise l'affichage d'une matrice   *
 * sur la sortie standard.                                  *
 *                                                          *
 * nb_lignes : Le nombre de lignes de la matrice.           *
 * nb_colonnes : Le nombre de colonnes de la matrice.       *
 * m : La matrice a afficher.                               *
 ************************************************************/

void affiche_matrice(int nb_lignes, int nb_colonnes, float m[nb_lignes][nb_colonnes])
{
	
	//--------------------------------------
	//	INSERER ICI VOTRE CODE...
	//--------------------------------------

}
