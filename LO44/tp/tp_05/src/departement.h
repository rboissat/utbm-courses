/**********************************************
* Librairies                                  *
**********************************************/

#include <stdio.h>
#include <stdlib.h>

/**********************************************
* Définition du type de donnée département    *
**********************************************/

typedef struct d {
	char code_region[3];
	char code_departement[3];
	char libelle[100];
	int pop;
} departement;

/**********************************************
* Prototypes des fonctions relatives au type  *
* de donnée département                       *
**********************************************/

void affiche_departement(departement d);
int compare_nom(departement *dep1, departement *dep2);
