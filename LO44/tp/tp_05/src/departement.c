/**********************************************
* Fonctions relatives au type de donnée       *
* département                                 *
**********************************************/
#include <string.h>

#include "departement.h"


/**********************************************
* Affichage sur la sortie standard            *
**********************************************/
void affiche_departement(departement d)
{
  printf("code region : %s | ",d.code_region);
  printf("code departement : %s | ",d.code_departement);
  printf("nom : %s | ",d.libelle);
  printf("population : %d\n\n",d.pop);
}

int compare_nom(departement *dep1, departement *dep2) {
  if (strcmp(dep1->libelle,dep2->libelle) == 0) {
    return 0;
  } else { (strlen(dep1->libelle) > strlen(dep2->libelle)) ? 1 : -1; }
}
