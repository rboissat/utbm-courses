#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "liste_chainee.h"

/**********************************************
* Programme de gestion d'une liste de         *
* départements                                *
**********************************************/

/**********************************************
* Prototype des fonctions                     *
**********************************************/
List charge_departements();
void affiche_departements_alpha(char lettre, List l);
void affiche_departement_pop_maxi(List l);
void affiche_departement_pop_mini(List l);
void gestion_erreur(void);
void demande_utilisateur(char *query, List query_list);


/**********************************************
* Programme principal                         *
**********************************************/
int main(int argc, char *argv[])
{
  if (argc != 2) {
    gestion_erreur();
  } else {
    // Chargement de la liste des départements
    List liste_dep;
    liste_dep = charge_departements();

    affiche_liste(tri_departements_sur_noms(liste_dep));
    demande_utilisateur(argv[1], liste_dep);

    return 0;
  }

}

/**********************************************
* Fonction permettant d'afficher les          *
* départements dont le nom débute par un      *
* caractère donné                             *
**********************************************/
void affiche_departements_alpha(char lettre, List l)
{
  lettre = toupper(lettre);
  fprintf(stdout,"Départements commençant par la lettre %c:\n",lettre);
  while (l->next != NULL) {
    if (l->dep->libelle[0] == lettre)
        affiche_departement(*(l->dep));
    l=l->next;
  }
}


/**********************************************
* Fonction permettant d'afficher le           *
* départements dont la population est la plus *
* grande                                      *
**********************************************/
void affiche_departement_pop_maxi(List l)
{
  List max = (List) malloc(sizeof(element));
  int pop_max = 0;

  fprintf(stdout,"Département ayant la plus grande population:\n");

  while (l->next != NULL) {
    //test population departement et tampon
    if (l->dep->pop > pop_max) {
      //si pop departement >, tampon = pop_departement
      pop_max=l->dep->pop;
      //repere pour savoir quel departement afficher
      max=l;
    }
    l=l->next;
  }
//affichage du departement
  affiche_departement(*(max->dep));
}


/**********************************************
* Fonction permettant d'afficher le           *
* départements dont la population est la plus *
* petite                                      *
**********************************************/
void affiche_departement_pop_mini(List l)
{
  List min = (List) malloc(sizeof(element));
  int pop_min=99999999; //initialisation du min à 99999999

  while (l->next != NULL) {
    if (l->dep->pop<pop_min) {
      pop_min=l->dep->pop;
      min=l;
    }
    l=l->next;
  }
  affiche_departement(*(min->dep));
}

/**********************************************
* Fonction de gestion d'erreur                *
*                                             *
**********************************************/
void gestion_erreur(void) {
  fprintf(stderr,"Erreur ! Usage: ./departement [+hab | -hab | Lettre]\n");
  exit(1);
}

/**********************************************
* Fonction de gestion de demande utilisateur  *
*                                             *
**********************************************/
void demande_utilisateur(char *query, List query_list) {
  if (strcmp(query,"+hab") == 0) {
    affiche_departement_pop_maxi(query_list);
  } else if (strcmp(query,"-hab") == 0) {
    affiche_departement_pop_mini(query_list);
  } else if (strlen(query) != 1) {
    gestion_erreur();
  } else {
    affiche_departements_alpha((char)query[0], query_list);
  }
}


/**********************************************
* Fonction permettant de charger la liste des *
* départements                                *
**********************************************/
List charge_departements()
{

	FILE* fp; 			// Fichier contenant la liste des départements
	int buffer_len = 101;		// Taille du buffer
	char buffer[buffer_len];	// Buffer de lecture
	departement *dep_ptr;		// Element courant
	int nb_departements;		// Nombre de départements
	int i;				// Boucle
	List l=NULL;			// Liste des departements
	
	// Ouverture du fichier
	if((fp = fopen("departements.txt", "r")) == NULL)
	{
		printf("Le fichier n'a pas pu être ouvert.\n");
		exit(1);
	}
	
	// Initialisation du buffer de lecture
	buffer[100] = '\0';

	// Lecture du nombre de departements
	fgets(buffer, 100, fp);
	if (buffer == NULL)
	{
		printf("Le fichier est corrompu.\n");
		fclose(fp);
		exit(1);
	}
	nb_departements = atoi(buffer);
	

	// Lecture jusqu'a la fin du fichier
	for (i=0; i<nb_departements; i=i+1)
	{
		// Création d'un département
		dep_ptr = malloc(sizeof(departement));
		// Lecture du code region
		fgets(buffer, 100, fp);
		if (buffer == NULL)
		{
			printf("Le fichier est corrompu.\n");
			fclose(fp);
			exit(1);
		}
		dep_ptr->code_region[0] = buffer[0];
		dep_ptr->code_region[1] = buffer[1];
		dep_ptr->code_region[2] = '\0';
		// Lecture du code departement
		fgets(buffer, 100, fp);
		if (buffer == NULL)
		{
			printf("Le fichier est corrompu.\n");
			fclose(fp);
			exit(1);
		}
		dep_ptr->code_departement[0] = buffer[0];
		dep_ptr->code_departement[1] = buffer[1];
		dep_ptr->code_departement[2] = '\0';
		// Lecture du nom du departement
		fgets(buffer, 100, fp);
		if (buffer == NULL)
		{
			printf("Le fichier est corrompu.\n");
			fclose(fp);
			exit(1);
		}
		strcpy(dep_ptr->libelle, buffer);
		// Lecture de la population
		fgets(buffer, 100, fp);
		if (buffer == NULL)
		{
			printf("Le fichier est corrompu.\n");
			fclose(fp);
			exit(1);
		}
		dep_ptr->pop = atoi(buffer);
		// Ajout dans la liste
		l=insertion_queue(l, dep_ptr);
	}

	// Fermeture du fichier
	fclose(fp);
	return l;
}




