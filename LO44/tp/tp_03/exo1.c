/*____________________________________________________________________________
 * TP3 - Exercice 1
 * Romain BOISSAT
 *____________________________________________________________________________*/

#include <stdio.h>
#include <stdlib.h>

int main( int argc , char* argv[] )
{
  int j,k;
  int *ptr; //Déclaration d'un pointeur sur un entier
  j = 1;
  k = 2;
  ptr = &k; //Affectation de l'adresse de k à la valeur du pointeur ptr
  printf("%d\n",j); //Affichage de la valeur de j
  printf("%d\n",k); //Affichage de la valeur de k
  printf("%p\n",ptr); //Affichage de la valeur de *ptr, ici adresse de k
  printf("%p\n",(void *) &j); //Affichage de l'adresse de j
  printf("%p\n",(void *) &k); //Affichage de l'adresse de k
  printf("%p\n",(void *) &ptr); //Affichage de l'adresse de *ptr

  return EXIT_SUCCESS;
}
