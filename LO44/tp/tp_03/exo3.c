/*____________________________________________________________________________
 * TP 3 - Exo 3: Allocation dynamique de mémoire
 * Romain BOISSAT
 *____________________________________________________________________________*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// Déclaration du type booléen
typedef enum {
  FALSE = 0,
  TRUE
} BOOLEAN;

// Prototypes
int * rempli_aleatoirement(int *tab, int taille);
void affiche_tableau(int *tab, int taille);
int * tri_bulle(int *tab, int taille);

int main( int argc,char *argv[] )
{
  int *myTab;
  int N;

  // Générateur d'entropie
  srand(time(NULL));

  // Demande de saisie utilisateur
  printf("Saisissez le nombre d'éléments N : ");
  scanf("%d", &N);

  // Création du tableau
  myTab = malloc( N * sizeof(int) );

  // Gestion d'erreur
  if( myTab == NULL )
  {
    fprintf(stderr,"Allocation impossible");
    exit(EXIT_FAILURE);
  }

  // Remplissage aléatoire
  rempli_aleatoirement( myTab, N);

  // Affichage du tableau initial
  affiche_tableau( myTab, N);

  // Tri du tableau
  tri_bulle( myTab, N);

  // Affichage du tableau trié
  affiche_tableau( myTab, N);

  // Libération de la mémoire
  free(myTab);

  // Sortie du programme
  return EXIT_SUCCESS;

}

int * rempli_aleatoirement(int *tab, int taille)
{
  int i;

  for( i=0; i < taille; i++)
  {
    tab[i] = ( rand()%10 );
  }

  return tab;

}

void affiche_tableau(int *tab, int taille)
{
  int i;

  printf("[ %d", tab[0]);
  for( i=1; i < taille; i++)
  {
    printf(" | %d", tab[i]);
  }
  printf(" ]\n");

}

int * tri_bulle(int *tab, int taille)
{
  int i; // index du tableau
  int j; // incrémentation de la boucle de test
  int tmp; // variable de stockage
  BOOLEAN est_trie = FALSE ; // on suppose le tableau non trié

  for( i=0; i < taille; i++ && est_trie == FALSE)
  {
    est_trie = TRUE;

    for( j=1; j < (taille-i); j++)
    {
      // Si tab[i-1] est plus grand que tab[i], on les swap
      if( tab[j-1] > tab[j] )
      {
        tmp = tab[j-1];
        tab[j-1] = tab[j];
        tab[j] = tmp;

        // Le tableau n'est toujours pas trié
        est_trie = FALSE;
      }
    }
  }

  return tab;

}
