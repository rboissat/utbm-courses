/*____________________________________________________________________________
 * TP3 - Exercice 2
 * Romain BOISSAT
 *
 * Testeur de palindrome (ex: radar)
 *____________________________________________________________________________*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Prototypes
int palindrome(char *word);

int main(int argc, char* argv[])
{

  char testWord[100];
  int result;

  printf("Saisissez un mot : ");
  scanf("%s", testWord);

  // On appelle palindrome() en affectant à result le code de retour
  result = palindrome(testWord);

  if( result == 1 )
    printf("'%s' est un palindrome\n", testWord);
  else
    printf("'%s' n'est pas un palindrome\n", testWord);

  return EXIT_SUCCESS;
}

int palindrome(char *word)
{
  int i = 0;
  int size = 0;
  int is_true = 1; // On suppose que c'est un palindrome

  size = strlen(word); // Détermination de la taille du mot passé en paramètre

  while( i < size && is_true == 1 )  // Arrêt dès la première lettre différente
  {
    if( word[i] != word[(size-1) - i] )
      is_true = 0;

    i = i++;
  }

  return is_true;
}
