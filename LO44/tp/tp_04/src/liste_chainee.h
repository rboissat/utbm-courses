/**********************************************
* Librairie pour la gestion d'une liste       *
* chainee de departement.                     *
**********************************************/

#include <stdio.h>
#include <stdlib.h>
#include "departement.h"

/**********************************************
* Définition du type de booléen               *
**********************************************/

#define TRUE 1
#define FALSE 0
typedef int BOOL;

/**********************************************
* Définition de la liste chainée              *
**********************************************/

typedef struct elm {
	departement *dep;
	struct elm *next;
} element;

typedef element * List;

/**********************************************
* Fonctions sur liste chainees                *
**********************************************/

// Test si la liste est vide
BOOL vide(List l);

// Insertion d'éléments
List insertion_tete(List l, departement *dep);
List insertion_queue(List l, departement *dep);

// Suppression d'éléments
List suppression_tete(List l);
List suppression_queue(List l);

// Affichage de la liste
void affiche_liste(List l);

// Compte le nombre d'éléments
int compte(List l);

// Tri par sélection
List tri_departements_sur_noms(List l);
