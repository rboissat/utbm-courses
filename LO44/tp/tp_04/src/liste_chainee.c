/**********************************************
* Fonctions relatives au type de donnée       *
* liste chainée                               *
**********************************************/

#include "liste_chainee.h"


/**********************************************
* Test si la liste est vide                   *
**********************************************/
BOOL vide(List l)
{
  if (l == NULL)
    return TRUE;
  else
    return FALSE;
}

/**********************************************
* Insertion en tete                           *
**********************************************/
List insertion_tete(List l, departement *dep)
{
  List tmp;
  tmp = (List) malloc(sizeof(element));
  tmp->dep = dep;
  tmp->next = l;

  return tmp;
}


/**********************************************
* Insertion en queue                          *
**********************************************/
List insertion_queue(List l, departement *dep)
{
  List tmp,p;
  tmp = (List) malloc(sizeof(element));
  tmp->dep = dep;
  tmp->next=NULL;

  if (l == NULL) {
    return tmp;

  } else {

    p = l;
    while (p->next != NULL)
      p = p->next;

    p->next = tmp;

    return l;
  }
}


/**********************************************
* Suppression en tete                         *
**********************************************/
List suppression_tete(List l)
{
  List tmp;

  if (l == NULL) {
    return l;
  } else {
    tmp = l;
    tmp = tmp->next;
    free(l);

    return tmp;
  }
}

/**********************************************
* Suppression en queue                        *
**********************************************/
List suppression_queue(List l)
{
  List tmp;

  if (l == NULL) {
    return l;
  } else {
    tmp = l;
    while (tmp->next->next != NULL)
      tmp = tmp->next;

    free(tmp->next);
    tmp->next = NULL;

    return tmp;
  }
}

/**********************************************
* Affichage de la liste                       *
**********************************************/
void affiche_liste(List l)
{
  if (l == NULL) {
    fprintf(stderr,"La liste est vide\n");
  } else {
    while (l->next != NULL) {
      fprintf(stdout,"%s - %s | pop: %d\n",
              l->dep->code_departement,
              l->dep->libelle,
              l->dep->pop);

      l = l->next;
    }
  }
}

/**********************************************
* Compte le nombre d'éléments                 *
**********************************************/
int compte(List l)
{
  int nbe = 0;

  if (l == NULL) {
    return 0;
  } else {
    while (l->next != NULL) {
      nbe++;
      l = l->next;
    }
  }
  return nbe;
}
/**********************************************
* Tri par sélection                           *
**********************************************/
List tri_departements_sur_noms(List l) {

  List sorted_list = (List) malloc(sizeof(element));
  List mini = (List) malloc(sizeof(element));

    return sorted_list;
}
