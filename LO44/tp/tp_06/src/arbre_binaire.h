/******************************************************************************
 * LO44
 * TP 6 - Arbres Binaires et Récursivité
 * Romain BOISSAT
 * Fichier 'arbre_binaire.c'
 *
 * Programme manipulant la structure d'arbre binaire.
 * Primitives de bases récursives.
 ******************************************************************************/
#define CREER_ARBRE() (ArbreBinaire) malloc(sizeof(Node))

/* Types */
typedef enum {
  FALSE = 0,
  TRUE
} Booleen;

typedef struct node {
  struct node *fg;
  int valeur;
  struct node *fd;
} Node;

typedef Node* ArbreBinaire;

/* Prototypes */
Booleen est_vide(ArbreBinaire a);
Booleen est_feuille(ArbreBinaire a);
ArbreBinaire enraciner(ArbreBinaire a, ArbreBinaire gauche, ArbreBinaire droit);
ArbreBinaire nouvelle_feuille(int v);
int valeur_noeud(ArbreBinaire a);
ArbreBinaire sous_arbre_droit(ArbreBinaire a);
ArbreBinaire sous_arbre_gauche(ArbreBinaire a);

int compte_noeuds(ArbreBinaire a);
int compte_feuilles(ArbreBinaire a);
Booleen contient(ArbreBinaire a, int val);
int profondeur(ArbreBinaire a);
int node_sum(ArbreBinaire a);
