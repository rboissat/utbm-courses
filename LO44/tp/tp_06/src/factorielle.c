/******************************************************************************
 * LO44
 * TP 6 - Arbres Binaires et Récursivité
 * Romain BOISSAT
 * Fichier 'factorielle.c'
 *
 * Programme interactif de calcul de factorielle. Algorithme récursif.
 ******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* On calcule avec des unsigned int pour contrer la limitation du type int
 * Sinon nous sommes limités à un résultat en 32bits signé soit
 * 2 147 483 638. On affiche alors au max factorielle 16 = 2 004 189 184
 * avant l'overflow */

/* Prototypes */
int is_int(char *string);
void usage(void);
unsigned int factorielle(int n);
unsigned int factorielle_iterative(int n);

int main(int argc, char *argv[]) {
  /* contrôle des arguments d'entrée */
  if (argc != 3)
    usage();
  else if (is_int(argv[2]) != 0)
    usage();

  if (strcmp(argv[1],"-r") ==0)
    fprintf(stdout,"%d\n",factorielle(atoi(argv[2])));
  else
    fprintf(stdout,"%d\n",factorielle_iterative(atoi(argv[2])));

  return 0;
}

int is_int(char *string) {
  int pos;
  /* On ne traite que les entiers naturels */
  if (string[0] == '-')
    return 1;
  /* On vérifie que la chaine soit bien composée de chiffres en base 10 */
  for (pos = 0; string[pos] != '\0'; pos++)
    if (string[pos] < '0' || string[pos] > '9')
      return 1;
  return 0;
}

void usage(void) {
  fprintf(stderr,"Usage: ./factorielle [METHODE: -r (recursive) ou \
-i (iterative) ][ENTIER NATUREL]\n");
  exit(1);
}

unsigned int factorielle(int n) {
  unsigned int r = 0;

  if (n == 1 || n == 0)
    r += 1;
  else
    r = r + n * factorielle(n-1);

  return r;
}

/* Algo de factorielle_interative *
 *
 * ALGO factorielle_interative
 * n : entier naturel
 * r : entier naturel
 *  DEBUT
 *    r <- 1
 *    TANT_QUE n >= 0 FAIRE
 *      r <- r * n
 *      n <- n - 1
 *    FIN_TANT_QUE
 *  FIN
 */
unsigned int factorielle_iterative(int n) {
  unsigned int r = 1;

  while (n > 0) {
    r = n * r;
    n--;
  }

  return r;
}

