/******************************************************************************
 * LO44
 * TP 6 - Arbres Binaires et Récursivité
 * Romain BOISSAT
 * Fichier 'arbre_binaire.c'
 *
 * Programme manipulant la structure darbre binaire.
 * Primitives de bases récursives.
 ******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include "arbre_binaire.h"

Booleen est_vide(ArbreBinaire a) {
  if (a == NULL)
    return TRUE;
  else
    return FALSE;
}

Booleen est_feuille(ArbreBinaire a) {
  if (est_vide(a) == TRUE) {
    fprintf(stderr,"EST_FEUILLE: Erreur ! ARbre vide\n");
    exit(1);
  } else if (est_vide(a->fg) && est_vide(a->fd)) {
    return TRUE;
  } else {
    return FALSE;
  }
}

ArbreBinaire enraciner(ArbreBinaire a,ArbreBinaire gauche,ArbreBinaire droit) {
  if (est_vide(a) == TRUE) {
    fprintf(stderr,"ENRACINER: Erreur ! La racine est vide\n");
    exit(1);
  } else {
    a->fg = gauche;
    a->fd = droit;
    return a;
  }
}

ArbreBinaire nouvelle_feuille(int v) {
  ArbreBinaire f = CREER_ARBRE();
  f->valeur = v;
  f->fg = NULL;
  f->fd = NULL;

  return f;
}

int valeur_noeud(ArbreBinaire a) {
  if (est_vide(a) == TRUE) {
    fprintf(stderr,"VALEUR_NOEUD: Erreur ! Arbre vide\n");
    exit(1);
  } else {
    return a->valeur;
  }
}

ArbreBinaire sous_arbre_droit(ArbreBinaire a) {
  if (est_vide(a) == TRUE || est_feuille(a) == TRUE) {
    fprintf(stderr,"SOUS_ARBRE_DROIT: Erreur ! Arbre vide ou feuille\n");
    exit(1);
  } else {
    return a->fd;
  }
}

ArbreBinaire sous_arbre_gauche(ArbreBinaire a) {
  if (est_vide(a) == TRUE || est_feuille(a) == TRUE) {
    fprintf(stderr,"SOUS_ARBRE_DROIT: Erreur ! Arbre vide ou feuille\n");
    exit(1);
  } else {
    return a->fg;
  }
}

int compte_noeuds(ArbreBinaire a) {
  int nbn= 0;

  if (est_vide(a) == TRUE) {
    nbn = 0;
  } else {
    nbn = compte_noeuds(a->fg) + 1 + compte_noeuds(a->fd);
  }
  return nbn;
}

int compte_feuilles(ArbreBinaire a) {
  int nbf = 0;

  if (est_feuille(a) == TRUE) {
    nbf = 1;
  } else {
    nbf = compte_feuilles(a->fg) + compte_feuilles(a->fd);
  }
  return nbf;
}

Booleen contient(ArbreBinaire a, int val) {
  if (est_vide(a) == TRUE)
    return FALSE;
  else if (valeur_noeud(a) == val)
    return TRUE;
  else /* OU Logique */
    return (contient(a->fg, val) || contient(a->fd, val));
}
/* BONUS */
int profondeur(ArbreBinaire a) {
  int prof = 0;

  if (est_vide(a) == TRUE)
    return prof;

  if (compte_noeuds(a->fg) > compte_noeuds(a->fd)) {
    prof = 1 + profondeur(a->fg);
  } else {
    prof = 1 + profondeur(a->fd);
  }
}

int node_sum(ArbreBinaire a) {
  int NS = 0;

  /*if (est_vide(a) == TRUE) {
    NS = 0;

  } else */ if (est_feuille(a) == TRUE) {
    NS = a->valeur;

  } else {
    NS = a->valeur + node_sum(a->fg) + node_sum(a->fd);
  }

  return NS;
}




