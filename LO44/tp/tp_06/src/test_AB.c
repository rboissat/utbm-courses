/******************************************************************************
 * LO44
 * TP 6 - Arbres Binaires et Récursivité
 * Romain BOISSAT
 * Fichier 'arbre_binaire.c'
 *
 * Programme manipulant la structure d'arbre binaire.
 * Primitives de bases récursives.
 ******************************************************************************/
#include <stdio.h>
#include <stdlib.h>

#include "arbre_binaire.h"

int main(int argc, char *argv[]) {
  fprintf(stdout,"\t\tProgramme de manipulation d'arbres binaires\n");

  /* Test des primitives
  ArbreBinaire a = NULL;

  ArbreBinaire b = CREER_ARBRE();
  b->valeur = 10;
  b->fg, b->fd = NULL;

  ArbreBinaire c = CREER_ARBRE();
  c->valeur = 10;
  c->fg, c->fd = NULL;

  printf("Arbre a est vide ? (0: FALSE | 1: TRUE) %d\n", est_vide(a));
  printf("Arbre c est vide ? (0: FALSE | 1: TRUE) %d\n", est_vide(c));

  ArbreBinaire racine = CREER_ARBRE();
  ArbreBinaire big = CREER_ARBRE();
  if (big = enraciner(racine,b,c))
    fprintf(stdout,"Enracinement réussi\n");
  Fin test */

  /* Construction de l'arbre  7   n0 ******************************************
   *                        /   \
   *                  n10  5     6  n11
   *                      / \   / \
   *            n20 n21  1   2 3   4  n22 n23
   ****************************************************************************/

  /* Affichage de l'arbre */
  fprintf(stdout,"\n\
          \t\t\t         7\n\
          \t\t\t       /   \\\n\
          \t\t\t      5     6\n\
          \t\t\t     / \\   / \\\n\
          \t\t\t    1   2 3   4\n\n");

  /* On construit les feuilles de l'arbre */
  ArbreBinaire n0   = nouvelle_feuille(7);
  ArbreBinaire n10  = nouvelle_feuille(5);
  ArbreBinaire n11  = nouvelle_feuille(6);
  ArbreBinaire n20  = nouvelle_feuille(1);
  ArbreBinaire n21  = nouvelle_feuille(2);
  ArbreBinaire n22  = nouvelle_feuille(3);
  ArbreBinaire n23  = nouvelle_feuille(4);

  /* On construit l'arbre demandé */
  ArbreBinaire final  = CREER_ARBRE();
  final = enraciner(n0,enraciner(n10,n20,n21),enraciner(n11,n22,n23));

  /* on vérifie que l'arbre demandé ne soit pas nul */
  printf("Vide ? %s\n", (est_vide(final)) ? "TRUE" : "FALSE");
  /* On compte le nombre de noeuds */
  fprintf(stdout,"L'arbre construit a %d noeud(s).\n",compte_noeuds(final));
  /* On compte le nombre de feuilles */
  fprintf(stdout,"L'arbre construit a %d feuille(s).\n",compte_feuilles(final));
  /* On vérifie si la valeur 1 est dans l'arbre */
  fprintf(stdout,"Contient la valeur '1' ? %s\n",
          (contient(final,1)) ? "TRUE" : "FALSE");
  /* On vérifie si la valeur 9 est dans l'arbre */
  fprintf(stdout,"Contient la valeur '9' ? %s\n",
          (contient(final,9)) ? "TRUE" : "FALSE");
  /* BONUS : On mesure la profondeur de l'arbre */
  fprintf(stdout,"Profondeur de l'arbre: %d\n",profondeur(final));

  /* Calcul de la somme des noeuds */
  fprintf(stdout,"Somme des Noeuds = %d\n",node_sum(final));

  /* On libère la mémoire occupée par l'arbre */
  free(final);

  return 0;
}
