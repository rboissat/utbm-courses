var map;

var fe_lng = [];
var fe_lat = [];

var pa_lng = [];
var pa_lat = [];

var k = 1;

var polylines = [];
var n = 0;

var interval;
var geofence;

var inZone = false;

function initialize() {
  if (GBrowserIsCompatible()) {
    map = new GMap2(document.getElementById("gmap"));
    map.setCenter(new GLatLng(47.64397867, 6.852172666667), 15);
    map.setUIToDefault();
    parseFencePoints();
  }
}

function parseFencePoints() {
    $(document).ready(function() {
        $.ajax({ type: "GET", url: "fence_data.xml", dataType: "xml",
            success: function(xml) {
                var i = 0;
                $(xml).find("coordinates").each(function() {
                    fe_lat[i] = $(this).find("lat").text();
                    fe_lng[i] = $(this).find("lng").text();
                    i++;
                });
                showGFence();
                parseWayPoints();
            }
        });
    });
}

function parseWayPoints() {
    $(document).ready(function() {
        $.ajax({ type: "GET", url: "path_data.xml", dataType: "xml",
            success: function(xml) {
                var i = 0;
                $(xml).find("coordinates").each(function() {
                    pa_lat[i] = $(this).find("lat").text();
                    pa_lng[i] = $(this).find("lng").text();
                    i++;
                });
                interval = setInterval("showPoints()", 1000);
            }
        });
    });
}

GPolygon.prototype.Contains = function(point) {
  var j = 0;
  var oddPoints = false;
  var x = point.lng();
  var y = point.lat();
  for (var i=0; i < this.getVertexCount(); i++) {
    j++;

    if (j == this.getVertexCount()) {j = 0;}

    lat_i = this.getVertex(i).lat()
    lat_j = this.getVertex(j).lat()
    lng_i = this.getVertex(i).lng()
    lng_j = this.getVertex(j).lng()

    if (((lat_i < y) && (lat_j >= y)) || ((lat_j < y) && (lat_i >= y))) {
      if ( lng_i + (y-lat_i)/(lat_j-lat_i)*(lng_j-lng_i) < x ) {
        oddPoints = !oddPoints
      }
    }
  }
  return oddPoints;
}

function showGFence() {
    var i = 0;
    var points = []
    for (i = 0; i < fe_lat.length; ++i) {
        points.push(new GLatLng(fe_lat[i], fe_lng[i]));
    }
    geofence = new GPolygon(points, "#FF3333", 1, 0.8, "#FF3333", 0.5);
    map.addOverlay(geofence);
}

function showPoints() {
    lastPoint = new GLatLng(parseFloat(pa_lat[k-1]), parseFloat(pa_lng[k-1]));
    point = new GLatLng(parseFloat(pa_lat[k]), parseFloat(pa_lng[k]));

    addPolyline(lastPoint, point);

    map.setCenter(point);

    k++;

    if (geofence.Contains(point) && !inZone) {
        document.getElementById("pos").innerHTML = "Inside";
        inZone = true;
    } else if (!geofence.Contains(point) && inZone) {
        document.getElementById("pos").innerHTML = "Outside";
        inZone = false;
    }

    if (k == pa_lat.length) {
        clearInterval(interval);
    }
}

function addPolyline(lastPoint, point) {
    points = [];
    points[0] = lastPoint;
    points[1] = point;

    polylines[n] = new GPolyline(points);
    map.addOverlay(polylines[n]);
    n++;

}

function reset() {
    clearInterval(interval);
    for (j = 0; j < n; ++j) {
        map.removeOverlay(polylines[j]);
    }
    n = 0;
    i = 1;
    interval = setInterval("showPoints()", 1000);
}
