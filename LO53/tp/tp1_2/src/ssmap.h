/*  LO53:     TP1_2
 *  Author:   Romain BOISSAT
 *  File:     ssmap.h
 *
 */

#ifndef SSMAP
#define SSMAP
#include <map>
#include <vector>

#include "point.h"

using namespace std;

struct ReferencePoint {
  Point coordinates;
  map<string, float> measurements;
};

vector<ReferencePoint> parseFile(const char*);
const float SssDistance(map<string, float>&, ReferencePoint&);
Point getClosestInSss(map<string, float>&, vector<ReferencePoint>&);

#endif

