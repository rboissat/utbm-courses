/*  LO53:     TP1_2
 *  Author:   Romain BOISSAT
 *  File:     point.h
 *
 */

#ifndef POINT_H
#define POINT_H

#include <iostream>
#include <cmath>

using namespace std;

// "Point" structure
class Point {
  private:
    float Xpt;
    float Ypt;
    float Zpt;

  public:
    // Constructors
    Point(void);
    Point(float, float);
    Point(const Point&);

    // Destructors
    ~Point();

    // Getters/Setters
    float getX(void);
    float getY(void);
    float getZ(void);
    void setX(float);
    void setY(float);
    void setZ(float);

    // Operators
    Point &operator=(const Point&);
    friend ostream& operator<< (ostream&, Point&);
};

// Euclidean Distance
float euclid_dist(Point &a, Point &b);

#endif

