/*  LO53:     TP1_2
 *  Author:   Romain BOISSAT
 *  File:     friis.h
 *
 */

#ifndef FRIIS
#define FRIIS

#include <map>
#include <vector>
#include "point.h"

using namespace std;

// Client Antenna Gain
#define CAG 0.0
// Light speed
#define LS  299792458.0

struct AccessPoint {
 Point coordinates;
 string mac_address;
 float antenna;
 float output_power;
 unsigned long frequency;
};

struct CalibrationPoint {
 Point coordinates;
 map<string, float> measurements;
};

map<string, float> \
FbcmCalibration(const vector<AccessPoint> &aps, \
                const vector<CalibrationPoint> &rps, \
                const float cli_antenna_gain);


map<string, float> \
FbcmDistances(const map<string, float> &measurements, \
              const vector<AccessPoint> &aps, \
              const map<string, float> &friis_indices);

#endif

