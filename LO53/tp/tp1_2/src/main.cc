/*  LO53:     TP1_2
 *  Author:   Romain BOISSAT
 *  File:     main.cc
 *
 */

#include <iostream>
#include <fstream>
#include <cmath>
#include <vector>
#include <map>
#include "friis.h"
#include "ssmap.h"

using namespace std;

float min_test; 
float min_ssmap;

int main (int argc, char *argv[]) {
  vector<AccessPoint> aps;
  vector<CalibrationPoint> rps;

  // Initialisation
  AccessPoint tmp;

  float c[5][2] = { {4.93,25.81}, {4.83,10.88},
                    {20.05,28.31}, {4.13,7.085},
                    {5.74,30.35}};

  string m[5] = {"00:13:CE:95:E1:6F","00:13:CE:95:DE:7E",
                 "00:13:CE:97:78:79", "00:13:CE:8F:77:43",
                 "00:13:CE:8F:78:D9"};

  float a[5] = {5.0, 5.0, 5.0, 5.0, 5.0};

  float o[5] = {20.0, 20.0, 20.0, 20.0, 20.0};

  unsigned long f[5] = {2417000000u, 2417000000u, 2417000000u, 2417000000u,
                        2417000000u};

  for (int i=0; i < 5; i++) {
    tmp.coordinates.setX(c[i][0]);
    tmp.coordinates.setY(c[i][1]);
    tmp.mac_address = m[i];
    tmp.antenna = a[i];
    tmp.output_power = o[i];
    tmp.frequency = f[i];

    aps.push_back(tmp);
  }

  cout << "LO53: TP1 : FbcmCalibration" << endl;
  cout << "-------------------------------------" << endl;
  map<string, float> friis_calibrated = FbcmCalibration(aps, rps, CAG);

  map<string, float>::iterator pos;
  for (pos = friis_calibrated.begin(); pos != friis_calibrated.end(); ++pos) {
    cout  << "key: \"" << pos->first << "\" "
          << "value: " << pos->second << endl;
    }
  cout << "-------------------------------------" << endl;
  cout << "LO53: TP2 : SSmap" << endl;
  cout << "-------------------------------------" << endl;

  Point pt;
  int nb_pt = 1;
  float max = 0, err = 0, avg = 0;
  vector<ReferencePoint> ssmap = parseFile("../files/input.csv");
  vector<ReferencePoint> data = parseFile("../files/test.csv");
  vector<ReferencePoint>::iterator it;

  cout << ssmap.size() << " imported points in SSmap" << endl;
  cout << data.size() << " imported points to locate" << endl;

  ofstream file("results.csv", ios::out);
  file << "Real coords. \t Coords. found \t Error(m)" << endl;

  for(it = data.begin() ; it != data.end() ; it++){
    pt = getClosestInSss(it->measurements,ssmap);
    err = euclid_dist(pt, it->coordinates);

    file << it->coordinates << " \t " << pt << " \t " << err << endl;
    cout << it->coordinates << " \t " << pt << " \t " << err << endl;
    avg += err;
    nb_pt++;
    if (max < err)
      max = err;
  }
  file.close();
  avg /= nb_pt;

  cout << "Max error : "<< max << endl;
  cout << "Average error : "<< avg << endl;

  return 0;
}

