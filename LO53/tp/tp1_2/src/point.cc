/*  LO53:     TP1_2
 *  Author:   Romain BOISSAT
 *  File:     point.cc
 *
 */

#include "point.h"

using namespace std;

Point::Point(void) {
  Xpt = 0.0;
  Ypt = 0.0;
  Zpt = 0.0;
}

Point::Point(float x, float y) {
  Xpt = x;
  Ypt = y;
  Zpt = y;
}

Point::Point(const Point &src) {
  Xpt = src.Xpt;
  Ypt = src.Ypt;
  Zpt = src.Zpt;
}

Point::~Point() { return; }

// Getters/Setters
float Point::getX(void) { return Xpt; }
float Point::getY(void) { return Ypt; }
float Point::getZ(void) { return Zpt; }
void Point::setX(float new_X) { Xpt = new_X; }
void Point::setY(float new_Y) { Ypt = new_Y; }
void Point::setZ(float new_Z) { Zpt = new_Z; }

// Operators
Point& Point::operator=(const Point &src) {
  Xpt = src.Xpt;
  Ypt = src.Ypt;
  Zpt = src.Zpt;
  return *this;
}

ostream& operator<< (ostream& o, Point& p) {
  o << "(" << p.Xpt << ", " << p.Ypt << ", " << p.Zpt << ")";
  return o;
}

float euclid_dist(Point &a, Point &b) {
  return sqrt(pow(a.getX() - b.getX(), 2) +
              pow(a.getY() - b.getY(), 2) +
              pow(a.getZ() - b.getZ(), 2));
}

