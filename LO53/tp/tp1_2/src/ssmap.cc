/*  LO53:     TP1_2
 *  Author:   Romain BOISSAT
 *  File:     ssmap.cc
 *
 */

#include <fstream>
#include <cstring>
#include <cstdlib>
#include <cmath>

#include "ssmap.h"

#define MAXLINE  512

vector<ReferencePoint> parseFile(const char* path) {
  fstream fstr;
  char lbuff[MAXLINE];
  vector<ReferencePoint> v_rp;

  fstr.open(path, fstream::in);
  if(fstr.fail())
    exit(EXIT_FAILURE);

  while(fstr.getline(lbuff, MAXLINE)) {
    char *strbuf;
    ReferencePoint rp;
    rp.coordinates = Point();

    for(int i = 0;; i++) {
      if (i==0) {
        strbuf = strtok(lbuff, ";");
        rp.coordinates.setX(atof(strbuf));
      } else if (i==1) {
        strbuf = strtok(NULL, ";");
        rp.coordinates.setY(atof(strbuf));
      } else if (i==2) {
        strbuf = strtok(NULL, ";");
        rp.coordinates.setZ(atof(strbuf));
      } else {
        strbuf = strtok(NULL, ";");
        if(!strbuf) break;
        rp.measurements[strbuf] = atof(strtok(NULL, ";"));
      }
    }
    v_rp.push_back(rp);
  }

  fstr.close();
  return v_rp;
}

const float SssDistance(map<string, float> &measurements, ReferencePoint &rp) {
  float res = 0;

  map<string, float>::iterator me_i(measurements.begin()),
                               me_e(measurements.end());

  for(; me_i != me_e; me_i++)
      res += pow(measurements.find(me_i->first)->second -
             rp.measurements.find(me_i->first)->second, 2);

  return sqrt(res);
}

Point getClosestInSss(map<string, float> &measurements,
                      vector<ReferencePoint> &ssmap)
{
  Point pt;
  float sum = 0,
        espilon = 0.0000000001,
        min = 10000,
        tmp;
  int i,
      mini = 0;

  for (i = 0; i < ssmap.size(); i++) {
    tmp = SssDistance(measurements, ssmap[i]);
    if (min > tmp) {
      min = tmp;
      mini = i;
    }
  }
  pt = ssmap[mini].coordinates;

  return pt;
}

