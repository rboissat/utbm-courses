/*  LO53:     TP1_1
 *  Author:   Romain BOISSAT
 *  File:     friis.cc
 *
 */

#include <map>
#include <cmath>
#include "friis.h"

using namespace std;

map<string, float> \
FbcmCalibration(const vector<AccessPoint> &aps, \
                const vector<CalibrationPoint> &rps, \
                const float cli_antenna_gain)
{
  vector<AccessPoint> tmp_aps = aps;
  vector<CalibrationPoint> tmp_rps = rps;

  map<string, float> result;
    int j = 0;
    vector<AccessPoint>::iterator ap, ap_e;
    vector<CalibrationPoint>::iterator cp, cp_e;

      for(ap = tmp_aps.begin(), ap_e = tmp_aps.end(); ap != ap_e; ap ++) {
        result[ap->mac_address] = 0.0;

        for(cp = tmp_rps.begin(), cp_e = tmp_rps.end(); cp != cp_e; cp ++) {
          j++;

          result[ap->mac_address] += ((ap->output_power 
                                      - cp->measurements[ap->mac_address]
                                      + ap->antenna 
                                      + cli_antenna_gain
                                      + 20 * log10(LS / ap->frequency)
                                      - 20 * log10(4 * M_PI))
                                      / (10 * log10(euclid_dist(ap->coordinates,
                                                                cp->coordinates))));
      }
      result[ap->mac_address] /= j;
   }
  return result;
}

map<string, float> \
FbcmDistances(const map<string, float> &measurements, \
              const vector<AccessPoint> &aps, \
              const map<string, float> &friis_indices,
              float cli_antenna_gain){
    map<string, float> result;

    vector<AccessPoint> tmp_aps = aps;
    map<string, float> temp_measurements = measurements;
    map<string, float> tmp_friis_indices = friis_indices;
    vector<AccessPoint>::iterator ap, ap_e;

    for(ap = tmp_aps.begin(), ap_e = tmp_aps.end(); ap != ap_e; ++ap) {
      result[ap->mac_address] = (ap->output_power 
                                 - temp_measurements[ap->mac_address]
                                 + ap->antenna
                                 + cli_antenna_gain
                                 + 20 * log10(LS / ap->frequency)
                                 - 20 * log10(4 * M_PI))
                                 / (10 * tmp_friis_indices[ap->mac_address]);
    }
  return result;
}

