#include <iostream>
#include "point.h"

using namespace std;

int main (int argc, char *argv[]) {
  cout << "debug\n";
  Point n = Point();

  Point m = n;
  Point p(12.3,23.4);
  cout << n << m << "\n";
  n.setX(10.2);
  cout << n << "\n";
  cout << p << "\n";
  return 0;
}

