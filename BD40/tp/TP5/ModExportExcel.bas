Option Compare Database
Option Explicit

Public Sub ExportXLS_tblMedecin()
'----------
' D�claration des objets Access : V�rifier la r�f�rence � la biblioth�que DAO
'----------
Dim dbs As DAO.Database
Dim rst As DAO.Recordset
Dim fld As DAO.Field

'----------
' D�claration des objets Excel : R�f�rencer la bibiloth�que Microsoft Excel 9, 10 Objetcs Library
'----------
Dim appExcel      As Excel.Application
Dim classeurExcel As Excel.Workbook
Dim feuilleExcel  As Excel.Worksheet

On Error Resume Next
Dim strpath As String
strpath = "c:\"
Dim strnomtab As String
strnomtab = "Medecin"

'----------
' Variables de boucles
'----------
Dim intLig As Integer
Dim intCol As Integer

'----------
' Cr�ation du classeur
'----------
Set appExcel = CreateObject("Excel.Application")
Set classeurExcel = appExcel.Workbooks.Add
Set feuilleExcel = appExcel.ActiveSheet
appExcel.Visible = True

'----------
' Ouverture de la table des produits
'----------

Set dbs = CurrentDb
Set rst = dbs.OpenRecordset(strnomtab)

'----------
' Remplissage de la feuille active
'----------
With feuilleExcel
   '
   ' Cr�ation de la premi�re ligne avec le nom des colonnes
   '
     intCol = 1
    For Each fld In rst.Fields
        .Cells(1, intCol).Value = fld.Name
    intCol = intCol + 1
    Next fld
   
   '
   ' boucle de parcours des enregistrements et ajout d'une ligne par enregistrement
   '
   intLig = 2
   Do While Not rst.EOF
      intCol = 1
      For Each fld In rst.Fields
        .Cells(intLig, intCol).Value = fld.Value
        intCol = intCol + 1
      Next fld
      intLig = intLig + 1
      rst.MoveNext
   Loop
   
   '
   ' nom de la feuille Excel
   '
   .Name = "liste des " & strnomtab

End With

'----------
' Enregistrement du classeur : sur c:\
'----------
classeurExcel.SaveAs strpath & strnomtab & ".xls"

'Fermeture de Excel
appExcel.Quit

End Sub