Option Compare Database
Option Explicit

Private Sub btnAnnuler_Click()
DoCmd.Close
End Sub

Private Sub btnParcourir_Click()
Dim fd As FileDialog
On Error Resume Next
Set fd = FileDialog(msoFileDialogOpen)
fd.Title = "S�lectionnez une base de donn�es..."
fd.AllowMultiSelect = False
With fd.Filters
    .Add "Bases de donn�es", "*.mdb"
    .Add "Fichiers MDE", "*.mde"
    .Add "Tous les fichiers", "*.*"
End With
If fd.Show = True Then
    Me.txtBase = fd.SelectedItems(1)
End If
Set fd = Nothing
End Sub

Private Sub btnProtect_Click()
If IsNull(Me.txtBase) Then
    MsgBox "S�lectionnez une base de donn�es au pr�alable !", vbInformation
    Exit Sub
End If
If Dir(Me.txtBase) = "" Then
    MsgBox "Fichier introuvable !", vbExclamation
    Exit Sub
End If
DefinirToucheMAJ Me.txtBase, (Me.optProtection = 1)
If Me.optProtection = 1 Then
    MsgBox "Touche Majuscule activ�e", vbInformation
Else
    MsgBox "Touche Majuscule d�sactiv�e", vbInformation
End If
End Sub

