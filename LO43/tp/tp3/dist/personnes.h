#include <string>
using namespace std;

class personne {
	protected:
		string nom;
		string prenom;
                string universite;
                int age;
	public:
		personne(void);
                personne(string, string, string, string, int);

                virtual ~personne();

                const string getNom();
                const string getPrenom();
                virtual const string getUniversite();
                virtual const string getQualite(void);
                virtual const string getExperience(void);
};

void personne::affiche() {
	cout << "mon nom est " << nom <<" et mon prenom " << prenom;
}

...

class etudiant : public personne {
	private :
		// donnees specifiques a etudiant
		string branche;
		string filiere;

	public :
	        const string getQualite(void);
                etudiant (string, string, string, string, string, int );
                const void print(void);
                void setBranche(const string);
                const string getBranche(const string);
                void setFiliere(const string);
                const string getFiliere(const string);
    		void affiche(void);
};
