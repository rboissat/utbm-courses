#include "etudiant.h"
#include <iostream>

etudiant::etudiant(string newNom, string newPrenom, string newUniv, string newSpec, string newBranche,int newAge)
{
    
    setNom(newNom);
    setPrenom(newPrenom);
    setEtablissement(newUniv);
    branche=newBranche;
    filiere=newSpec;
}

const void etudiant::print(void)
{
	cout << "Je m'appelle" << getPrenom() << " " << getNom() <<
                ", j'étudie en " << getBranche() << " et je suis spécialisé en "
                << getFiliere() << endl;
}

const string etudiant::getBranche(void)
{
	return branche;
}

const string etudiant::getFiliere(void)
{
	return filiere;
}

void etudiant::setBranche(const string newBranche)
{
	branche = newBranche;
}

void etudiant::setFiliere(const string newSpec)
{
	filiere = newSpec;
}

const string etudiant::getQualite(void)
{
    return "Etudiant";
}
