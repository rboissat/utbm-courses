#include <string>
using namespace std;
#ifndef PERSONNE_H
#define PERSONNE_H

class personne {
  protected:
    string prenom;
    string nom;
    string universite;
    int age;

  public:
    personne(void);
    personne (string, string, string, int);

    virtual ~personne();

    virtual const string getQualite(void); 
    const int getAge(void);
    void setAge(const int);
    const string getNom(void);
    const string getPrenom(void);
    const string getUniversite(void);
    virtual const void print(void);
    void setNom(const string);
    void setPrenom(const string);
    void setEtablissement(const string);
};
#endif
