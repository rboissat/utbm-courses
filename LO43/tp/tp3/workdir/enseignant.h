#include "personne.h"
#ifndef TEACHER_H
#define TEACHER_H
class enseignant : public personne
{
  private :
    string departement;
    string lab;
  public :
    enseignant (string, string, string, string, string, int);
    const void print(void);
    void setDep(const string);
    const string getDep(void);
    void setLab(const string);
    const string getLab(void);
    const string getQualite(void);
};
#endif
