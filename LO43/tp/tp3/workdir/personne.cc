#include "personne.h"
#include <iostream>
personne::personne(void) {
  nom = "john";
  prenom = "doe";
  universite = "UTBM";
  age = 0;
}

personne::personne(string newNom, string newPrenom, string newUniv, int newAge) {
  nom = newNom;
  prenom = newPrenom;
  universite = newUniv;
  age = newAge;
}

personne::~personne(void) {}

const string personne::getQualite(void) {
    return "Personne";
}

const void personne::print (void) {
  cout <<
    "Je m'appelle " << getPrenom() << " " << getNom() <<
    " et j'ai " << getAge() << "ans" <<endl;

  cout << "Mon établissement: " << getUniversite() << "\n" << endl;
}

const int personne::getAge (void) {
  return age;
}

void personne::setAge (int newAge) {
  age = newAge;
}

const string personne::getNom (void) {
  return nom;
}

const string personne::getPrenom (void) {
  return prenom;
}

const string personne::getUniversite (void) {
  return universite;
}

void personne::setNom(const string newNom) {
  nom = newNom;
}

void personne::setPrenom(const string newPrenom) {
  prenom = newPrenom;
}

void personne::setEtablissement(const string newUniversite) {
  universite = newUniversite;
}
