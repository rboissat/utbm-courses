#include "enseignant.h"
#include <iostream>

enseignant::enseignant( string newNom, string newPrenom, string newUniv,
                        string newDep, string newLab,int newAge) {
    setNom(newNom);
    setPrenom(newPrenom);
    setEtablissement(newUniv);
    lab=newLab;
    departement=newDep;
}

const void enseignant::print(void) {
  cout << "Je m'appelle " << getPrenom() << " " << getNom() <<
          ", je travaille au labo " << getLab() << " et j'enseigne en "
          << getDep() <<endl;
}

const string enseignant::getDep(void) {
  return departement;
}

const string enseignant::getLab(void) {
  return lab;
}

void enseignant::setDep(const string newDep) {
  departement = newDep;
}

void enseignant::setLab(const string newLab) {
  lab = newLab;
}
const string enseignant::getQualite(void) {
    return "Enseignant Chercheur";
}
