#ifndef _DB
#define _DB
#include <list>
#include "personne.h"
#include "etudiant.h"
#include "enseignant.h"

class db
{
  private:
    list<personne *> database;
    list<personne *>::iterator itor;

  public:
    db(void) {};
    ~db(void) {};
    void add(personne);
    void remove(int);
    int ageStats(void);
    void getQualites(void);
};
#endif
