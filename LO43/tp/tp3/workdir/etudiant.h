#include "personne.h"
#ifndef ETUDIANT_H
#define ETUDIANT_H
class etudiant : public personne {
  private :
    // donnees specifiques a etudiant
    string branche;
    string filiere;

  public :
    etudiant (string, string, string, string, string, int);
    const string getQualite(void); 
    const void print (void);
    void setBranche(const string);
    const string getBranche(void);
    void setFiliere(const string);
    const string getFiliere(void);
};
#endif
