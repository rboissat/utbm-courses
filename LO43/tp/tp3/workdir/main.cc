#include <iostream>
#include <string>
#include "personne.h"
#include "etudiant.h"
#include "enseignant.h"
#include "db.h"

using namespace std;

int
main(void)
{
  db BDD;

  personne stud("BOISSAT","Romain","GI - UTBM",21);

  //BDD.add(stud);

  stud.print();

  stud.setNom("RISS");
  stud.setPrenom("Aline");
  stud.setAge(19);
  stud.setEtablissement("TC - UTBM");
  stud.print();

  enseignant p("KMIOTEK","Pawel","UTBM","GI","SeT",28);
  p.print();
}
