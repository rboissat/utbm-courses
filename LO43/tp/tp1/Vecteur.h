#ifndef VECTEUR_H
#define VECTEUR_H
//
// Type vecteur
//
#include <iostream>
using namespace std;
//
// Type vecteur de int
//
class vecteur
{
private:

  int taille;
  float *elements;

public:

 // Constructeurs par defaut(void) , avec la taille du vect.(int), par copie(vecteur&)

// Constructeurs par defaut(void) , avec la taille du vect.(int), par copie(vecteur&)
  vecteur();
  vecteur(int);
  vecteur(const vecteur&);

// Destructeur
  ~vecteur();
// Acces a la taille du vecteur
  int Taille(void);

// acces au element []
  float& operator[](int);

// affectation : =(vecteur), =(int)
  vecteur &operator=(float);
  vecteur &operator=(const vecteur&);

// incrementation/decr. : ++(void), ++(int), --(void)
  // préfixe
  vecteur &operator++(void);
  // suffixe
  vecteur &operator++(const int);

  vecteur &operator--(void);
  vecteur &operator--(const int);

// op. booleens : ==(vecteur&), <, >, <=, >=
  bool operator==(const vecteur&);
  bool operator<(const vecteur&);
  bool operator>(const vecteur&);

// op. binaires : +(vecteur&), -
  vecteur operator+(const vecteur&);
  vecteur operator-(const vecteur&);

// produit scalaire: *(vecteur&)
  vecteur operator*(const vecteur&);

// produit par un scalaire: v * n, n * v
  vecteur operator*(float);
  friend vecteur operator*(float, vecteur&);

//vecteur operator*(int);
  vecteur operator*(int);
  friend vecteur operator*(int, vecteur&);

// auto-adition : +=(vecteur&), -=
  vecteur &operator+=(const vecteur&);
  vecteur &operator-=(const vecteur&);

// Operateurs d'entree/sortie: <<, >>
  friend ostream& operator<< (ostream&, vecteur&);
  friend istream& operator>> (istream&, vecteur&);

};

#endif // VECTEUR_H
