#include <iostream>

// Include de l'application
#include "Vecteur.h"
using namespace std;

int
main(int argc, char **argv)
{

	cout << "PROGRAMME DE TEST" << endl;

	// Constructeurs
	vecteur v1(3);
	vecteur v2 = v1;
        vecteur v3;
        vecteur v4(5);
        vecteur v5(v1);
        vecteur v6;

        cout << "Taille de v4 : " << v4.Taille() << endl;

	cout << "v5 :	" << v5 << endl;
	v1[0] = 2;
	v1[1] = 4;
	v1[2] = 6;
        v5 = v1;
	cout << "v1 :	" << v1 << endl;
	cout << "v5 :	" << v5 << endl;
	
	v2 = v1;
	cout << "v2 = v1 :	" << v2 << endl;

	v2++;
	cout << "v2++ :	" << v2 << endl;
	
	v3 = v1 + v2;
	cout << "v3 = v1 + v2 :	" << v3 << endl;

    v6 = v1 * 2;
    cout << "v6 = v1 * 2 :" << v6 << endl;

	cout << "SORTIE PROGRAMME DE TEST" << endl;
}

