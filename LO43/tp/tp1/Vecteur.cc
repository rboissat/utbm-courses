// Type vecteur
#include "Vecteur.h"
using namespace std;

// Constructeurs
vecteur::vecteur(void)
{
  taille = 0;
  elements = NULL;
}

vecteur::vecteur(int length)
{
  int i;
  taille = length;
  elements = new float[taille];
  for (i=0;i<taille;i++)
    elements[i] = 0.;
}

vecteur::vecteur(const vecteur &source)
{
  int i;
  taille = source.taille;
  elements = new float[taille];
  for (i=0;i<taille;i++)
    elements[i] = source.elements[i];
}

// Destructeur
vecteur::~vecteur()
{
  if(elements)
  {
    delete elements;
  }
}
// Nombre d'elements
int vecteur::Taille(void)
{
  return taille;
}

// Acces aux elements
float& vecteur::operator[](int i)
{
  if(i<taille && i>=0)
  {
    return elements[i];
  }
  else
  {
    throw "Index Error: Out of range";
  }
}

// affectation : =(vecteur), =(int)
vecteur &vecteur::operator=(float nbreel)
{
  int i;
  for (i=0;i<taille;i++)
  {
    elements[i] = nbreel;
  }
  return *this;
}

vecteur &vecteur::operator=(const vecteur &source)
{
  int i;
  taille = source.taille;
  delete elements;
  elements = new float[taille];
  for (i=0;i<taille;i++)
    elements[i] = source.elements[i];

  return *this;
}

// incrementation/decr. : ++, ++(int), --
vecteur &vecteur::operator++(void)
{
  int i;
  for (i=0;i<taille;i++)
    elements[i]++;
}

vecteur &vecteur::operator++(int i)
{
  for (i=0;i<taille;i++)
    elements[i]++;
}

vecteur &vecteur::operator--(void)
{
  int i;
  for (i=0;i<taille;i++)
    elements[i]--;
}

vecteur &vecteur::operator--(int i)
{
  for (i=0;i<taille;i++)
    elements[i]--;
}

// op. booleens : ==, <, >, <=, >=
bool vecteur::operator==(const vecteur &source)
{
  int i;
  if(source.taille != taille)
    return false;

  for (i=0;i<taille;i++)
    if(source.elements[i] != elements[i])
      return false;

  return true;
}

bool vecteur::operator<(const vecteur &source)
{
  int i;
  if(source.taille < taille)
    return true;

  return false;
}

bool vecteur::operator>(const vecteur &source)
{
  int i;
  if(source.taille > taille)
    return true;

  return false;
}

// op. binaires : +, -
vecteur vecteur::operator+(const vecteur &source)
{
  int i;
  vecteur v(taille);
  for (i=0;i<taille;i++)
    v.elements[i] = source.elements[i] + elements[i];

  return v;
}

vecteur vecteur::operator-(const vecteur &source)
{
  int i;
  vecteur v(taille);
  for (i=0;i<taille;i++)
    v.elements[i] = source.elements[i] - elements[i];

  return v;
}

// produit scalaire : *
vecteur vecteur::operator*(const vecteur &source)
{
  int i;
  vecteur v(taille);
  for (i=0;i<taille;i++)
    v.elements[i] = elements[i] * source.elements[i];

  return v;
}

// produit par un scalaire: n * v, v * n
vecteur vecteur::operator*(const int scal)
{
  int i;
  vecteur v(taille);
  for (i=0;i<taille;i++)
    v.elements[i] = elements[i] * scal;

  return v;
}

// auto-adition : +=, -=
vecteur &vecteur::operator+=(const vecteur &source)
{
  int i;
  for (i=0;i<taille;i++)
    elements[i] += source.elements[i];
}

vecteur &vecteur::operator-=(const vecteur &source)
{
  int i;
  for (i=0;i<taille;i++)
    elements[i] -= source.elements[i];
}

// Operateurs d'entree/sortie: <<, >>
ostream& operator<< (ostream& o, vecteur& a)
{
  int i;
  o << "( ";
  for (i=0;i<a.Taille();i++)
    o << a.elements[i] << ' ';
  o << ")";

  return o;
}

istream& operator>> (istream& i, vecteur& a)
{
	return i;
}

