package controller;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import view.*;
import model.*;

public class PositionController implements MouseListener {

	private EnsemblePoints points;
	private AppliFrame frame;
	
	public PositionController(EnsemblePoints points, AppliFrame frame) {
		this.points = points;
		this.frame = frame;
		this.frame.addListener(this);
	}
	
	@Override
	public void mouseClicked(MouseEvent event) {
		points.addPoint(new Point(event.getX(), event.getY()));
		frame.update();
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}	
}
