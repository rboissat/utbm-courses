import controller.PositionController;
import view.AppliFrame;
import model.EnsemblePoints;

public class AppliMain {
	
	public static void main(String[] args) {
		EnsemblePoints model = new EnsemblePoints();
		
		AppliFrame frame = new AppliFrame(model);
		frame.setVisible(true);
		
		PositionController controller = new PositionController(model, frame);
	}
}
