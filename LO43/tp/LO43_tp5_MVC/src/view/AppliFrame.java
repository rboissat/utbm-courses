package view;

import java.awt.BorderLayout;
import java.awt.event.MouseListener;
import javax.swing.JFrame;

import model.EnsemblePoints;

public class AppliFrame extends JFrame {
	private static final long serialVersionUID = -1029498727820807969L;
	
	private EnsemblePoints points;
	private MapPanel map;
	private PositionLabel lab;
	
	public AppliFrame(EnsemblePoints points) {
		this.points = points;
		createIHM();
		update();
	}

	public void update() {
		this.repaint();
		lab.update();
	}

	private void createIHM() {
		map = new MapPanel(points);
		lab = new PositionLabel(points);
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.getContentPane().setLayout(new BorderLayout());
		this.getContentPane().add(map, BorderLayout.CENTER);
		this.getContentPane().add(lab, BorderLayout.NORTH);
		this.setSize(200, 200);
		this.setLocationRelativeTo(null);
	}
	
	public void addListener(MouseListener listener) {
		map.addMouseListener(listener);
	}
	
}
