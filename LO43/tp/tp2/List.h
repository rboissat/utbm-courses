// Creation of a generic class 'list' (template based) with one recursive
// algorithm
// File: List.h - HEADER
// Author: Romain BOISSAT
// Date : 24/03/2009

// Standard includes and namespace
#include <iostream>
using namespace std;

// Class List
template <class T> class List {
  // private data
  private:
    T value;
    List<T> *next;

  // Coplien class idiom
  public:
    // constructors
    List();
    List(const List<T>&);
    // destructor
    ~List(void);
    // basic operators
    List<T> operator=(const List<T>&);
    List<T> &operator+(T); // append a new element containing the arg value
    List<T> operator+(List<T>&);  // append one list to another
    List<T> operator-(int); // delete the i-th element
    T& operator[](int);
    // Some useful functions
    int Length(void); // return the length of the list
    void Print(void);
    // Recursive function
    // List<T> ReverseList(const List<T>&);
};

// Class 'List' C++ Code

// constructors
template <class T> List<T>::List() {
  value = 0;
  next = NULL;
}

template <class T> List<T>::List(const List<T> &source) {
  for (;next;) {
    List<T> *L = new List<T>;
    next = source.next;
    L->value = source.value;
    L->next = source.next;
    next = L->next;
  }
}

// destructor
template <class T> List<T>::~List(void) {
  for (;next;) {
    List<T> *next_elem = next;
    next = next->next;
    delete &next_elem;
  }
}

// basic operators

// =
template <class T> List<T> List<T>::operator=(const List<T> &source) {
  List<T> E = List<T>(source);
  return E;
}

// + (append a value to a list or concatenate two lists)
template <class T> List<T> &List<T>::operator+(T value) {
  List<T> *tmp = new List<T>;
  List<T> *current = this;
  tmp->value = value;

  if (!current)
    return *tmp;

  while (current->next)
    current = current->next;

  current->next = tmp;

  return *this;
}

template <class T> List<T> List<T>::operator+(List<T> &source)
{
    List<T> L(*this);
    List<T> M(source);
    List<T> *next_elem = next;

    for(;next_elem->next;)
        next_elem = next_elem->next;

    *next_elem->next = M;

    return L;
};

// [] extract a value from the list
template <class T> T& List<T>::operator[](int i) {
  int j;
  List<T> *next_elem = next;

  for (j = 0; j < i-1 && next_elem; j++)
    next_elem = next_elem->next;

  if (j == i-1)
    return next_elem->value;
  else
    cout << "Error: Index out of range" << endl;
}

// length
template <class T> int List<T>::Length(void)
{
    int i=1;
    // if there is no element then return 0
    if (!next)
      return 0;
    // else iterative length computation
    List<T> *next_elem = next;
    while (!next_elem->next)
    {
        i++;
        next_elem = next_elem->next;
    }
    return i;
};

// input-output
template <class T> void List<T>::Print(void)
{
    List<T> *next_elem = this;
    std::cout << "( " << value << ' ';
    for(;next_elem->next;)
    {
        std::cout << next_elem->value << ' ';
        next_elem = next_elem->next;
    }
    std::cout << ')' << std::endl;
};

//template <class T> List<T> ReverseList(const List<T>&) {
// Algorithm:
//START
//  IF is_empty(L) = true THEN
//    reverse <- L
//  ELSE
//    IF is_empty(rest(L)) = true THEN
//      reverse <- L
//    ELSE
//      reverse <- append(head(L), reverse(rest(L)))
//    ENDIF
//  ENDIF
//END
//
// With the folowing functions :
// * is_empty(L) = returns true if the L list is empty
// * rest(L) = returns the L list head truncated
// * append(L1,L2) = appends list L2 to list L1.
