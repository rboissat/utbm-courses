#include <iostream>

// Include de l'application
#include "List.h"

using namespace std;

int main(int argc, char **argv)
{

    cout << "PROGRAMME DE TEST" << endl;

	// Constructeurs
    List<int>l1;
    List<int>l2 = l1;

    l1.Print();
    cout << "taille de l1:" << l1.Length() << endl;
//    l2 = l2 + 5;
    l2.Print();

    
    cout << "Taille de l2 : " << l2.Length() << endl;

//    l2 = l2 + l1;
    
	cout << "l2 = l2 + l1 :	" ;
    l2.Print();

    cout << "l2[1] : " << l2[1] << endl;

    l2[0] = 2;
    l2[1] = 4;
    l2[2] = 6;
    cout << "l2 :	";
    l2.Print();
	
	l1 = l2;
	cout << "l1 = l2 :	" ;
	l1.Print();
	
	cout << "SORTIE TEST" << endl;
  
  return 0;
}

