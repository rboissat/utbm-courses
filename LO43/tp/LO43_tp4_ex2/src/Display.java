import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Display extends JFrame implements ActionListener {

	private static final long serialVersionUID = -4624735249793088187L;
	private String windowTitle = "Fenetre de base";
    private JButton button = new JButton("Bouton par défaut");
    private JLabel label = new  JLabel("Label par défaut");
    
	// Default Constructor
	public Display() {  
	  // Window is visible
         this.setVisible(true);
         
      // Default size
         this.setSize(300,100);	
      // Default Title
         this.setTitle(getWindowTitle());
         
      // Window is screen centered
         this.setLocationRelativeTo(null);
         
      // Exit the process when closed
         this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
         
      // Layout of the window
         this.setLayout(new BorderLayout()); 
         
      // Place the actionbutton on the top of the
         this.getContentPane().add(button, BorderLayout.NORTH);          
	 }

	// Copy Constructor
	public Display(String wTitle, String bAction, int wWidth, int wHeight, int defaultValue) {
        this.setVisible(true);
        this.setSize(wWidth,wHeight);
        this.setTitle(wTitle);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        this.setLayout(new BorderLayout());
        
      // Sets button's content
        button.setText(bAction);        
        this.getContentPane().add(button, BorderLayout.NORTH);
        
      // Sets text label's content
        label.setText(Integer.toString(defaultValue));
        this.getContentPane().add(label, BorderLayout.SOUTH);
        
      // Button's Listener
        button.addActionListener(this);
        
      // Debug Output
        System.out.println(defaultValue);
	}

	public void setWindowTitle(String windowTitle) {
		this.windowTitle = windowTitle;
	}

	public String getWindowTitle() {
		return windowTitle;
	}

	public void setButton(JButton button) {
		this.button = button;
	}

	public JButton getButton() {
		return button;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		int newVal = Integer.parseInt(label.getText()) + 1;
		label.setText(Integer.toString(newVal));
	}
}
