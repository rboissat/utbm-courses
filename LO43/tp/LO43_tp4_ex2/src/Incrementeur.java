public class Incrementeur {
	
	//Private Attribute
	private int valeur = 0;
	
	public void setValeur(int valeur) {
		this.valeur = valeur;
	}

	public int getValeur() {
		return valeur;
	}
	
	public void incrementValue() {
		this.valeur += 1;
	}
	
	public Incrementeur() {
		new Display("Incrémenteur","Incrémenter", 240, 80, getValeur());
	}
	
	public Incrementeur(int initialValue) {
		new Display("Incrémenteur","Incrémenter", 240, 80, initialValue);
	}
	
}
