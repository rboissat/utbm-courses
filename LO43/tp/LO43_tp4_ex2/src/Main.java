
public class Main {
    public static void main(String[] args) {
    	// Init without initial value
    	new Incrementeur();
    	// Init with initial value
    	new Incrementeur(20);
    }
}
