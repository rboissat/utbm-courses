package fr.utbm.lo43.tp6.area;

import java.awt.BasicStroke;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.JPanel;

public class AreaPanel extends JPanel implements Runnable {

	private static final long serialVersionUID = 1155684545875878373L;
	public static final int sleepTimeDuration = 100;
	private static final int border = 2;
	private RectArea area=null;
	
	
	public AreaPanel (RectArea area) {
		this.area = area;
	}

	
	public void paint (Graphics g) {
		super.paint(g);
		
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		
		Circle c;
		
		for (int i = 0; i<area.nbCercles();i++ ) {
			c = area.get(i);
			g2d.setColor(c.getCouleur());
			g2d.fillOval((int)(c.getPositionCentreX() - c.getRayon()), 
						 (int)(c.getPositionCentreY() - c.getRayon()), 
						 (int)c.getRayon()*2 - border,  // On gere le contour pour que l'affichage corresponde au modele
						 (int)c.getRayon()*2 - border);
			
			g2d.setColor(c.getCouleur().darker().darker());
			g2d.setStroke(new BasicStroke(border));
			g2d.drawOval((int)(c.getPositionCentreX() - c.getRayon()), 
						 (int)(c.getPositionCentreY() - c.getRayon()), 
						 (int)c.getRayon()*2 - border, 
						 (int)c.getRayon()*2 - border);
		}
		
	}
	
	
	@Override
	public void run() {
		for (int i = 0 ; i< 10000 ; i++) {
			this.repaint();
			try {
				Thread.sleep(sleepTimeDuration);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
