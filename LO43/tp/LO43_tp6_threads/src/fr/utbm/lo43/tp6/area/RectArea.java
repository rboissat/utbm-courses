package fr.utbm.lo43.tp6.area;

//import java.util.ArrayList;
import java.util.Vector;

public class RectArea {
	private double xMin;
	private double yMin;
	private double xMax;
	private double yMax;
	private Vector<Circle> cercles;
	
	public RectArea(double xMin, double yMin, double xMax, double yMax){
		this.xMin = xMin;
		this.yMin = yMin;
		this.xMax = xMax;
		this.yMax = yMax;
		cercles = new Vector<Circle>();
}
	
	public void ajout(Circle c) {
		if (recouvre(c) == false)
			this.cercles.add(c);
	}
	
	public int nbCercles() {
		return this.cercles.size();
	}
	
	public Circle get(int idx) {
		return this.cercles.elementAt(idx);
	}
	
	public boolean recouvre(Circle c) {
		boolean isCovering = false;
		Circle cercleCourant;
		
		for (int i = 0; i < this.cercles.size(); i++ ){
			cercleCourant = this.cercles.elementAt(i);
			
			if (c.recouvre(cercleCourant) == true)
				isCovering = true;
		}
		
		return isCovering;
	}
		
	// Cercles dans le canevas
	public boolean interieurCanevas(Circle c) {
		double xMinCercle = c.getPositionCentreX() - c.getRayon();
		double yMinCercle = c.getPositionCentreY() - c.getRayon();
		
		double xMaxCercle = c.getPositionCentreX() + c.getRayon();
		double yMaxCercle = c.getPositionCentreY() + c.getRayon();
		
		return (xMinCercle > xMin && yMinCercle > yMin && xMaxCercle < xMax && yMaxCercle < yMax) ? true : false;
	}
	
	public double getXMin() {
		return xMin;
	}
	public void setXMin(double min) {
		xMin = min;
	}
	public double getYMin() {
		return yMin;
	}
	public void setYMin(double min) {
		yMin = min;
	}
	public double getXMax() {
		return xMax;
	}
	public void setXMax(double max) {
		xMax = max;
	}
	public double getYMax() {
		return yMax;
	}
	public void setYMax(double max) {
		yMax = max;
	}
	public Vector<Circle> getCercles() {
		return cercles;
	}
	public void setCercles(Vector<Circle> cercles) {
		this.cercles = cercles;
	}
	
}
