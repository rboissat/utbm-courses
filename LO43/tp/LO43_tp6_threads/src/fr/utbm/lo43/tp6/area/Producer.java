package fr.utbm.lo43.tp6.area;

import java.awt.Color;
import java.util.Random;

public class Producer implements Runnable {
	private Color couleur;
	private boolean stopFlag;
	private RectArea zone;
	private double rayon;
	private Random rdm;
	// temps de sleep aléatoire
	public static final int sleepTimeDuration = new Random().nextInt() % 42 + 42;
	
	
	public Producer(Color c, RectArea z, double r) {
		this.couleur = c;
		this.rayon = r;
		this.zone = z;
		this.rdm = new Random();
	}
	
	@Override
	public void run() {
		while (stopFlag == false) {
			// Generation du cercle aleatoire
			double X = rdm.nextDouble() * zone.getXMax();
			double Y = rdm.nextDouble() * zone.getYMax();
			Circle tmpC = new Circle(X, Y, rayon, couleur);
			
			// Test du recouvrement et ajout 
			// Exclusion mutuelle de la RectAreas
			synchronized (zone) {
				if (!zone.recouvre(tmpC) && zone.interieurCanevas(tmpC)) {
					zone.ajout(tmpC);
				}
			}
			
			// Sleep
			try {
				Thread.sleep(sleepTimeDuration);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
		}
	}
	
	
}
