package fr.utbm.lo43.tp6.area;

import java.awt.Dimension;

import javax.swing.JFrame;

public class MainFrame extends JFrame{

	private static final long serialVersionUID = 8849904311394343999L;
	private AreaPanel panel;
	
	public MainFrame(RectArea area, int frameWidth, int frameHeight) {
		panel = new AreaPanel(area);
		
		this.setContentPane(panel);
		/** le undecorated permet de corriger un bug dans le test "interieurCanevas"
		 *	car le Window Manager applique un theme à la fenetre qui décale le ContentPanel vers le bas
		 *	Pour une question de praticité, j'ai augmenté de 40px la hauteur de la fenetre */
		//this.setUndecorated(true);
		// Marges pour compenser les dimensions du theme du Window Manager
		this.setSize(new Dimension(frameWidth+10,frameHeight+40));
		this.setTitle("LO43 - TP6: Threads");
		this.setVisible(true);
		// On kill les threads en fermant la frame
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		
		(new Thread(panel)).start();
	}
	
}
