package fr.utbm.lo43.tp6;

import java.io.IOException;

public class UnixPID{
/**
 * UnixPID - Simple class to get the PID of the java process
 * (Actually it is an ugly hack)
 * Source: http://blog.igorminar.com/2007/03/how-java-application-can-discover-its.html
 */
	private byte[] pid = null;
	
	public byte[] getPid() {
		return pid;
	}

	public UnixPID() {
		
		this.pid = new byte[100];
		String[] cmd = {"bash", "-c", "echo $PPID"};
		Process p = null;
		
		try {
			p = Runtime.getRuntime().exec(cmd);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  try {
			p.getInputStream().read(this.pid);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
