package fr.utbm.lo43.tp6.area;

import java.awt.Color;

public class Circle {
	private double positionCentreX;
	private double positionCentreY;
	private double rayon;
	private Color couleur;
	
	public Circle(){
		this(0.0, 0.0, 1.0, new Color(1,0,0));
	}
	
	public Circle(double x, double y, double r, Color c) {
		this.positionCentreX = x;
		this.positionCentreY = y;
		this.rayon = r;
		this.couleur = c;
	}
	
	// Teste si un cercle c recouvre le cercle instancié
	public boolean recouvre(Circle c) {
		double distance = Math.sqrt(Math.pow( (c.getPositionCentreX() - this.getPositionCentreX()), 2.0) +
				 					Math.pow((c.getPositionCentreY() - this.getPositionCentreY()), 2.0));

		return (c.getRayon() + this.getRayon() > distance) ? true : false;
	}

	public void setPositionCentreX(double positionCentreX) {
		this.positionCentreX = positionCentreX;
	}

	public double getPositionCentreX() {
		return positionCentreX;
	}

	public void setPositionCentreY(double positionCentreY) {
		this.positionCentreY = positionCentreY;
	}

	public double getPositionCentreY() {
		return positionCentreY;
	}

	public void setRayon(double rayon) {
		this.rayon = rayon;
	}

	public double getRayon() {
		return rayon;
	}

	public void setCouleur(Color couleur) {
		this.couleur = couleur;
	}

	public Color getCouleur() {
		return couleur;
	}
	
}
