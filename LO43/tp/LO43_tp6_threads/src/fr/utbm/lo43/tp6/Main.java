package fr.utbm.lo43.tp6;
/***
 * LO43 TP6 Threads Romain BOISSAT
 * LO43.TP6.Thread.Nom.Prenom.zip
 * @mailto nicolas.gaud@utbm.fr
 * @author rboissat
 * 
 */
import java.awt.Color;
import java.util.Random;

import fr.utbm.lo43.tp6.area.MainFrame;
import fr.utbm.lo43.tp6.area.Producer;
import fr.utbm.lo43.tp6.area.RectArea;

public class Main {

	/**
	 * @param args
	 */

	public static void main(String[] args) {
		// Définition des propriétés du programme
		final int frameWidth = 1000;
		final int frameHeight = 600;
		// Petite geekerie pour trouver le PID d'un process Java
		// bien que Java n'a pas du tout connaissance de cette notion
		final byte[] pid = new UnixPID().getPid();
		
		// Affectation des couleurs
		Color C1 = Color.RED;
		Color C2 = Color.BLUE;
		Color C3 = Color.ORANGE;
		Color C4 = Color.GREEN;
		
		int minRadius	= 20;
		int stepRadius  = 20; 
		
		// Affectation aléatoire des rayons
		int R1 = new Random().nextInt() % stepRadius + minRadius;
		int R2 = new Random().nextInt() % stepRadius + minRadius;
		int R3 = new Random().nextInt() % stepRadius + minRadius;
		int R4 = new Random().nextInt() % stepRadius + minRadius;
		
		RectArea area = new RectArea(0,0,frameWidth,frameHeight);
		@SuppressWarnings("unused")
		MainFrame frame	= new MainFrame(area,frameWidth,frameHeight);
		Producer P1 = new Producer(C1,area,R1);
		Producer P2 = new Producer(C2,area,R2);
		Producer P3 = new Producer(C3,area,R3);
		Producer P4 = new Producer(C4,area,R4);
		
		// Lancement des Threads
		(new Thread(P1)).start();
		(new Thread(P2)).start();
		(new Thread(P3)).start();
		(new Thread(P4)).start();
		// Avertissement en console
		System.out.print("Pour quitter le programme, stoppez le thread depuis la console d'eclipse, ou faites un kill "+new String(pid));
	}

}
