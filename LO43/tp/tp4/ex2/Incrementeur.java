public class Incrementeur {
	
	//Private Attribute
	private int valeur = 0;
	
	public void setValeur(int valeur) {
		this.valeur = valeur;
	}

	public int getValeur() {
		return valeur;
	}
	
	public void incrementValue() {
		this.valeur += 1;
	}
	
	public Incrementeur() {
		setValeur(0);
		new Display("Incrémenteur","Incrémenter", 240, 80, getValeur());
	}
	
	public Incrementeur(int initialValue) {
		setValeur(0);
		new Display("Incrémenteur","Incrémenter", 240, 80, initialValue);
	}
	
}
