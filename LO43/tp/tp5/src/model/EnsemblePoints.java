package model;
import java.awt.*;
import java.util.ArrayList;

public class EnsemblePoints {
	private ArrayList<Point> points;

	public EnsemblePoints () {
		points = new ArrayList<Point>();
	}
	
	public void addPoint(Point p) {
		points.add(p);
	}
	
	public Point getLastPoint() {
		return points.get(points.size() - 1);
	}
	
	public int getNbPoint() {
		return points.size();
	}
	
	public Point getPoint(int index) {
		return points.get(index);
	}
	
	public void setPoints(ArrayList<Point> points) {
		this.points = points;
	}

	public ArrayList<Point> getPoints() {
		return points;
	}
}
