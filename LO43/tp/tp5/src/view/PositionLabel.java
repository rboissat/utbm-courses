package view;
import java.awt.Point;

import javax.swing.*;
import model.EnsemblePoints;

public class PositionLabel extends JLabel {
	private static final long serialVersionUID = 6821450881545130092L;

	EnsemblePoints points;
	
	public PositionLabel(EnsemblePoints points) {
		this.points = points;
		this.setText(" x __  y __");
		update();
	}
	
	public void update() {
		if (points.getNbPoint() == 0) {
			this.setText(" x __  y __");
		} else {
			Point last = points.getLastPoint();
			this.setText(" x "+last.x+"  y "+last.y);
		}
	}
}
