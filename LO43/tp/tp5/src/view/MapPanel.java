package view;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import javax.swing.*;
import model.EnsemblePoints;

public class MapPanel extends JPanel {
	private static final long serialVersionUID = -3064498922890960331L;
	
	EnsemblePoints points;
	
	public MapPanel(EnsemblePoints points) {
		this.points = points;
	}
	
	public void paint(Graphics g) {
		super.paint(g);
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, g.getClipBounds().width, g.getClipBounds().height);
		if (points != null) {
			if (points.getNbPoint() != 0) {
				g.setColor(Color.BLUE);
				Point p;
				for (int i=0; i<points.getNbPoint(); i++) {
					p = points.getPoint(i);
					g.fillOval(p.x-1, p.y-1, 2, 2);
				}
			}
		}
	}
}
