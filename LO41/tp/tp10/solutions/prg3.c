/*-------------------------------------------------------
             prg2Bis.c : creation, attente terminaison
                         multi-threads + exclusion mutuelle

             compiler avec l'option -pthread
 --------------------------------------------------------*/

#include <pthread.h>
#include <stdio.h>

int  iGlobal = 1;
pthread_mutex_t mutex;
#define MarqueurFinArg -1
#define SortDuQuantum 100000   /* dimensionne pour ne pas permettre
                               son incrementation dans un seul
                               quantum, a modifier selon la charge
                               de la machine */
void* f (void* arg) 
  {
  int i=0;
  int *p = (int*) arg;  
     printf("Debut thread %d\n",p[0]);
     pthread_mutex_lock(&mutex);
     iGlobal++;
     pthread_mutex_unlock(&mutex);
     printf("\tiGlobal = %d\n",iGlobal);
     sleep(1);
  return NULL;
  }

main(int argc,char* argv[],char* arge[])
  {
  pthread_t *thread;
  int i,nbThreads;
  int *param;
  
  if (argc-1 != 1) fprintf(stderr,"%s nbThreads\n",argv[0]),exit(1);
  nbThreads = atoi(argv[1]);
  thread = (pthread_t*) malloc(sizeof(pthread_t)*nbThreads);
  param = (int*) malloc(sizeof(param)*nbThreads);
  pthread_mutex_init(&mutex,NULL);
  for (i=0;i<nbThreads;i++) {
     param[i]=i;
     if (pthread_create(&thread[i],NULL,f,&param[i])==-1)
        printf("pb pthread_create\n"),exit(1);
  }
  for (i=0;i<nbThreads;i++) {
     	pthread_join(thread[i],NULL);
	printf("thread %d termine\n",i);
  }
  printf("iGlobal = %d\n",iGlobal);
  }
  
