/*----------------------------------------------------------

           Le probleme des philosophes :
             threads et variables condition
             
             idee : un seul wait par philosophe
                    => 1 var. condition par philosophe
                    => condition attachee aux 2 baguettes
                    => fin de repas : signal sur chacune des var
                       conditions adjacentes

---------------------------------------------------------*/

#include <stdio.h>
#include <pthread.h>

#define NbPhilosophes 5
#define TempsMaxRepas 3

//------------ debut pack2age condition (termes a adapter a chaque pb)

typedef enum {Faux=0,Vrai} bool;
typedef struct {
  pthread_cond_t  cond;
  pthread_mutex_t mutex;  
  bool            baguetteLibre[NbPhilosophes]; /* pour chq philosophe i :
                                                      i   -> baguette gauche, 
                                                      i+1 -> baguette droite */
  }  tpack2;                         
typedef void (*tfonction1) (tpack2*);
typedef bool (*tfonction2) (tpack2*,int);
typedef void (*tfonction3) (tpack2*,int);

tpack2 pack2;

void pack2_init(tpack2 *ppack2,tfonction1 initTermes)
  { 
  initTermes(ppack2);
  pthread_cond_init(&pack2.cond,NULL);
  pthread_mutex_init(&pack2.mutex,NULL);
  }
  
void pack2_attendre(tpack2 *ppack2,tfonction2 propositionVraie,int i,tfonction3 action)
  {
  pthread_mutex_lock(&ppack2->mutex);  
  while (! propositionVraie(ppack2,i))
     pthread_cond_wait(&ppack2->cond,&ppack2->mutex); // deverrouille mutex pendant blocage
  if (action!=NULL) action(ppack2,i);
  pthread_mutex_unlock(&ppack2->mutex);  
  }

bool pack2_signalerTous(tpack2 *ppack2,tfonction3 action,int i,tfonction2 propositionVraie)
  {
  bool signalerEffectue = Faux;
  pthread_mutex_lock(&ppack2->mutex);
  if (action!=NULL) action(ppack2,i);
  if (propositionVraie(ppack2,i)) {
     pthread_cond_broadcast(&ppack2->cond);
     signalerEffectue = Vrai;
     }
  pthread_mutex_unlock(&ppack2->mutex);
  return signalerEffectue;
  }

  
//---------------- CODE PHILOSOPHE -----------------------

void initTermes(tpack2 *ppack2)
  { 
  int i;
  for (i=0;i<NbPhilosophes;i++) {
    ppack2->baguetteLibre[i] = Vrai;
    }
  }
  
bool  QlesDeuxBaguettesLibresPour(tpack2 *ppack2,int i)
  {
  int iPlus1=(i+1) % NbPhilosophes;
  return ppack2->baguetteLibre[i] && ppack2->baguetteLibre[iPlus1];
  }
  
void  prendrelesDeuxBaguettesPour(tpack2 *ppack2,int i)
  {
  int iPlus1=(i+1) % NbPhilosophes;
  ppack2->baguetteLibre[i]=ppack2->baguetteLibre[iPlus1]=Faux;
  }

void  libererlesDeuxBaguettesPour(tpack2 *ppack2,int i)
  {
  int iPlus1=(i+1) % NbPhilosophes;
  ppack2->baguetteLibre[i]=ppack2->baguetteLibre[iPlus1]=Vrai;
  }
   
void simule(int i)
  {
  printf("le philosophe %d commence a manger\n",i);
  sleep(rand()%TempsMaxRepas);
  printf("le philosophe %d a fini de manger\n",i);
  }

void* philosophe(void *p)
  {
  int i= * (int*) p;
  pack2_attendre(&pack2,QlesDeuxBaguettesLibresPour,i,prendrelesDeuxBaguettesPour);
     simule(i);
  pack2_signalerTous(&pack2,libererlesDeuxBaguettesPour,i,QlesDeuxBaguettesLibresPour); 
  return NULL; 
  }
  
//---------------------- MAIN ------------------------------

main(int argc,char* argv[])
  {
  pthread_t thread[NbPhilosophes]; 
  int i;
  int nom[NbPhilosophes];  // chq philosophe doit connaitre son nom
  
  srand(getpid());         // init generateur nb aleatoires     
  pack2_init(&pack2,initTermes);
  for (i=0;i<NbPhilosophes;i++) {
     nom[i]=i;
     pthread_create(&thread[i],NULL,philosophe,&nom[i]);  
     }
  for (i=0;i<NbPhilosophes;i++)
     pthread_join(thread[i],NULL);
  printf("-- TOUS LES PHILOSOPHES ONT MANGE --\n");
  }

