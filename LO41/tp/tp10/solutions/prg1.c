/*-------------------------------------------------------
             prg1.c : creation, attente terminaison

             compiler avec l'option -lpthread
 --------------------------------------------------------*/

#include <pthread.h>
#include <stdio.h>

int iGlobal = 1;

#define SortDuQuantum 1E8   /* dimensionne pour ne pas permettre
                               son incrementation dans un seul
                               quantum, a modifier selon la charge
                               de la machine */
#define MarqueurFinArg -1                               

void* f (void* arg) 
  {
  int i=0;
  int *p = (int*) arg;
  printf("thread 2 : iGlobal = %d  ",iGlobal);
  do  
     printf("arg[%d] = %d  ",i,p[i]);
  while (p[++i]!= MarqueurFinArg);
  printf("\n");
  while (iGlobal<SortDuQuantum) iGlobal++;
  printf("thread 2 : iGlobal = %8d\n",iGlobal);
  printf("thread 2 : terminaison\n");
  return (void*) iGlobal;             
  }

main(int argc,char* argv[],char* arge[])
  {
  pthread_t thread2;			 //declaration d'un thread
  int param[]={0,1,MarqueurFinArg};
  int codeRetour;
  if (pthread_create(&thread2,NULL,f,param)==-1) { //creation du thread
      printf("pb pthread_create\n"); exit(1);
  }
  printf("thread 1 : thread2 lance,    iGlobal = %d\n",iGlobal);

  sleep(1);
  printf("thread 1 : thread2 en cours, iGlobal = %8d\n",iGlobal);
  pthread_join(thread2,(void**) &codeRetour);
  printf("thread 1 : thread2 termine,  iGlobal = %8d   codeRetour = %d\n",iGlobal,codeRetour);
  }
  
