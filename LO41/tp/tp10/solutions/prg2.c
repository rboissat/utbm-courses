/*-------------------------------------------------------
             prg2.c : creation, attente terminaison
                      multi-threads

             compiler avec l'option -pthread
 --------------------------------------------------------*/

#include <pthread.h>
#include <stdio.h>

int iGlobal = 1;

#define MAX 20

void* f (void* arg) 
  {
  int *p = (int*) arg;
  while (iGlobal<MAX) {
     printf("thread%d : iGlobal = %d\n",p,++iGlobal);
     sleep(1);
  }
  return NULL;             /* bidon */
  }

int main(int argc,char* argv[],char* arge[])
  {
  pthread_t *thread;
  int i,nbThreads;
  
  if (argc-1 != 1) fprintf(stderr,"%s nbThreads\n",argv[0]),exit(1);
  nbThreads = atoi(argv[1]);
  thread = (pthread_t*) malloc(sizeof(pthread_t)*nbThreads);
  printf("iGlobal = %d\n",iGlobal);
  for (i=1;i<=nbThreads;i++) {
	if (pthread_create(&thread[i],NULL,f,(void*)i)==-1) {
        printf("pb pthread_create\n"); exit(1);
     }
  }
  for (i=1;i<=nbThreads;i++) 
     pthread_join(thread[i],NULL);
  printf("iGlobal = %d\n",iGlobal);
  exit(0);
  }
  
