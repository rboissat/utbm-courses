/*------------------------------------------------------------------

    Modele client-serveur : generation de nombres aleatoires, 
    -> on insere un  processus trieur entre le serveur et le client
       pour la reponse
    
    communication par files de messages, terminaison par Ctrl-C
    
    le serveur : le pere
    le trieur  : le premier fils cree
    un client  : un des autres processus fils (leur nombre est argv[1])
    
    un client emet un message de type 1 pour le serveur contenant : 
        (int pidEmetteur, int nbNombreDemandes, int valMaxNombre)
    ->  le serveur n'extrait que les messages de type 1
       
    le serveur emet un message de type 2 pour le trieur contenant :
        (int pidEmetteur, , int nbNombre, int tableau[nbNombreDemandes])
    ->  le trieur n'extrait que les messages de type 2
       
    le trieur emet un message de type pidClient pour le client contenant :
        (int tableau[nbNombreDemandes])
    -> un client n'extrait que les types de messages ayant son pid
    
    
    
    Remarque : version simplifiee -> pas de recouvrement
               numero interne connu avant creation fils
               
                
-------------------------------------------------------------------*/

#include <stdio.h>
#include <signal.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>

#define NBMAXNB 20

typedef struct {
        long  type;                   /* impose, type du msg */
        pid_t pidEmetteur;
        int   nbNombreDemandes;
        int   nMax;
        }
                  trequeteClient;
typedef struct {
        long  type;                   
        pid_t pidEmetteur;
        int   nbNombres;              /* doit etre fourni au trieur */
        int   tab[NBMAXNB];           /* on dimensionne au msg de taille max */
        }         treponseServeur;    

typedef struct {
        long  type;                   
        int   tab[NBMAXNB];           /* on dimensionne au msg de taille max */
        }         treponseTrieur;
       
        
            
int msgid ;            /* connu de tous les processus */

void initRand()        /* init generateur nb aleatoires */
  {
  srand(getpid());       
  }

void erreurFin(const char* msg) {perror(msg);exit(1);}

/*---------------- CODE CLIENT -----------------------*/

void constructionRequete(trequeteClient *preq)
  {
  preq->pidEmetteur      = getpid();
  preq->nbNombreDemandes = rand()%NBMAXNB;
  preq->nMax             = rand()+1;     /*evite 0*/
  }

void affichageRequete(trequeteClient *preq)
  {
  printf("\n%d nombres de 1 a %d demandes par le processus %d\n",
         preq->nbNombreDemandes,preq->nMax,preq->pidEmetteur);
  }
  
void affichageReponse(trequeteClient *preq,treponseTrieur *prep)
  {
  int i,lg;
  char msg[1000];
  lg=sprintf(msg,"\nNombres recus par le processus %d : \n",preq->pidEmetteur);
  for (i=0;i<preq->nbNombreDemandes;i++) 
     lg+=sprintf(msg+lg,"%d  ",prep->tab[i]) ;
  sprintf(msg+lg,"\n");
  write(1,msg,lg+1); 	/* assure l'indivisibilite de l'affichage */           
  }
  
void client()
  {
  trequeteClient req;
  treponseTrieur rep;
  int  tailleReq,tailleRep;

  tailleReq    = sizeof(trequeteClient) - sizeof(long);
  initRand();
  
  while (1) {
    constructionRequete(&req);
    affichageRequete(&req);
    req.type = 1;         /* num de processus utilisateur impossible */
    msgsnd(msgid,&req,tailleReq,0);
    tailleRep = req.nbNombreDemandes * sizeof(int);
    msgrcv(msgid,&rep,tailleRep,getpid(),0);
    affichageReponse(&req,&rep);
    }
  }
  
/*---------------- CODE SERVEUR -----------------------*/

void constructionReponseServeur(trequeteClient *preq,treponseServeur *prep)
  {
  int i;
  for (i=0;i<preq->nbNombreDemandes;i++)
     prep->tab[i] = rand()%preq->nMax;
  }
  
void serveur()
  {
  trequeteClient  req;
  treponseServeur rep;
  int  tailleReq,tailleRep;

  tailleReq    = sizeof(trequeteClient) - sizeof(long);
  initRand();
  
  while (1) {
     msgrcv(msgid,&req,tailleReq,1,0);
     constructionReponseServeur(&req,&rep);
     rep.type        = 2;
     rep.pidEmetteur = req.pidEmetteur;
     rep.nbNombres   = req.nbNombreDemandes;
     tailleRep = sizeof(pid_t) + sizeof(int) + req.nbNombreDemandes * sizeof(int);
     msgsnd(msgid,&rep,tailleRep,0);
     }
  }

/*---------------- CODE TRIEUR -----------------------*/


int ordreAscendantInt(int* v1, int* v2)
  {
  return *v1 - *v2;
  }
  
void constructionReponseTrieur(treponseServeur *preq,treponseTrieur *prep)
  {
  /*recopie du tableau resultat, puis tri croissant*/
  int i;
  for (i=0;i<preq->nbNombres;i++)  prep->tab[i]=preq->tab[i];
  qsort(prep->tab,preq->nbNombres,sizeof(int),ordreAscendantInt);
  }
  
void trieur()
  {
  treponseServeur req;                 /*correspond a la requete*/ 
  treponseTrieur  rep;
  int  tailleReq,tailleRep;

  tailleReq    = sizeof(treponseServeur) - sizeof(long);
   
  while (1) {
     msgrcv(msgid,&req,tailleReq,2,0);
     constructionReponseTrieur(&req,&rep);
     rep.type = req.pidEmetteur;
     tailleRep = req.nbNombres * sizeof(int);
     msgsnd(msgid,&rep,tailleRep,0);
     }
  }
  
  
    
/*------------- CODE GESTION PROCESSUS -----------------------*/

/*fonction permettant de creer nbFils qui executent la meme fonction*/

void forkn(int nbFils, void (*pauBoulot) ())
  {
  int i;
  for (i=0;i<nbFils;i++) 
     if (fork()==0) {
        (*pauBoulot) ();
        exit(0);         /*assure terminaison du fils*/
        }
  }
  
void traitantSIGINT(int s)
  {
  msgctl(msgid,IPC_RMID,NULL);
  exit(0);
  }

main(int argc,char* argv[])
  {
  int   nbClients;
  key_t cle;
  
  if (argc-1!=1) {
     fprintf(stderr,"Appel : %s <nb clients>\n",argv[0]);
     exit(2);
     }
  nbClients = atoi(argv[1]);
  if ((cle = ftok(argv[0],'0')) == -1)
     erreurFin("Pb ftok");
  if ((msgid = msgget(cle,IPC_CREAT  | IPC_EXCL | 0600)) == -1) 
     erreurFin("Pb msgget 1");
  forkn(1,trieur);
  forkn(nbClients,client);
  signal(SIGINT,traitantSIGINT);
  serveur(); 
  }
  
     
  
