/*-----------------------------------------------------------
               Les tubes (sans nom)
               le pere ecrit, le fils lit

               => A COMMENTER
-------------------------------------------------------------*/

#include <stdio.h>
#include <limits.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <signal.h>
#include <stdlib.h>


void erreur(const char* msg)      {perror(msg);exit(1);}


void traitantAbsenceLecteur (int num)
  {
  printf("passage par traitantAbsenceLecteur\n");
  exit(1);
  }

#define TailleTubeMax PIPE_BUF-1 /*5120 max # bytes atomic in write to a pipe */

main()
  {
  int pfd[2];
  char tampon[TailleTubeMax+1];

  // Creation du pipe
  if (pipe(pfd) == -1) erreur("pipe");

  // Fork
  switch (fork()) {
     case -1 : erreur("fork");
     case 0  :                     /* fils lecteur */                     
        // Fermeture descripteur d'écriture
        close(pfd[1]);
        printf("fils : attente RC clavier\n");
        // recuperation jusqu'à retour clavier char à char depuis stdin
        getchar();                             /* ? */
        // Lecture depuis le descripteur de lecture dans le tampon
        read(pfd[0],tampon,1);                 /* ? */
        sleep(1);                  /* assure 1 lecteur pour le 1er write */
        break;
     default :                     /* pere ecrivain */
        signal(SIGPIPE,traitantAbsenceLecteur);
        // fermeture du desc de lecture
        close(pfd[0]);
        // ecriture depuis tampon dans le desc d'écriture bloquée car taille_tampon < TailleTubeMax+1
        write(pfd[1],tampon,TailleTubeMax+1);
        printf("pere : %d car. ecrits dans tube\n",TailleTubeMax+1);
        // Attente terminaison fils
        wait(NULL);
        // Ecriture dans le desc d'écriture de "A"
        write(pfd[1],"A",1);
        break;
      }

    return EXIT_SUCCESS;
  }


