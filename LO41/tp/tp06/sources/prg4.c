/*____________________________________________________________________________
 * ps | wc -l
 *____________________________________________________________________________*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main( int argc, char *argv[])
{
  int pfd[2] = {0, 1};
  char *login = getlogin();
  char *buffer[5];

  fprintf(stdout,"Nombre de processus de l'utilisateur \"%s\" :",login);
  fflush(stdout); // forcer le vidage du buffer avant la fermeture de stdout

  if(pipe(pfd) == -1){ perror("erreur de pipe\n"); exit(1); }

  switch(fork())
  {
    case (pid_t) -1 :
      perror("fork borked\n"); exit(1);
    case (pid_t) 0 : // fils - execution de ps ux
      close(pfd[0]); // fermeture pfd lecture
      dup2(pfd[1],1); // duplication de pfd[1] vers stdout
      close(pfd[1]); // fermeture du descripteur redondant
      execlp("/bin/ps","ps","ux",NULL); // recouvrement
      perror("echec exec"); // si fail exec
      exit(1);
    default :
      close(pfd[1]); // fermeture pfd écriture
      dup2(pfd[0],0); // duplication de pfd[0] vers stdin
      close(pfd[0]); // fermeture du descripteur redondant
      execlp("/usr/bin/wc","wc","-l",NULL); // recouvrement
      perror("echec exec"); // si fail exec
      exit(1);
  }
  return EXIT_SUCCESS;
}
