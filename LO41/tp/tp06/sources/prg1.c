/*-----------------------------------------------------------
               Les tubes ordinaires
               le pere lit, le fils ecrit
-------------------------------------------------------------*/

#include <stdio.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

void erreur(const char* msg)      {perror(msg);exit(1);}

#define LGMAX 100

main()
  {
  int pfd[2];
  char tampon[LGMAX];

  if (pipe(pfd) == -1) erreur("pipe");

  switch (fork()) {
         case -1 : erreur("fork");
         case 0   :                   /* fils */
               // on ferme la lecture pour la synchro
               close(pfd[0]); 
               //copie de la char* dans buffer
               strcpy(tampon,"salut a toi"); 
               /*on écrit le contenu du buffer dans le descripteur d'écriture
               * en tenant compte de \0*/
               write(pfd[1],tampon,strlen(tampon)+1); 
               break;
        default  :                  /* pere */ 
               {
               char *p = tampon;
               // On ferme le descripteur d'écriture
               close(pfd[1]); 
               /* on concatène un à un les char de *tampon depuis le descripteur
                * de lecture dans tampon */
               while (read(pfd[0],p,1) != 0) p++; 
               // ou sinon
               //read(pfd[0],p,LGMAX);
               printf("chaine lue dans le tube : %s\n",tampon); // on affiche
               break;
               }
     }
    return EXIT_SUCCESS;
  }


