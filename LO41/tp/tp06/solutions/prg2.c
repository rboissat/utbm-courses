/*-----------------------------------------------------------
               Les tubes (sans nom)
               le pere ecrit, le fils lit
                 - Ecriture dans un tube plein
                 - Emission SIGPIPE en l'absence de lecteurs
-------------------------------------------------------------*/

#include <stdio.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <signal.h>


void erreur(const char* msg)      {perror(msg);exit(1);}

  
void traitantAbsenceLecteur (int num)
  {
  printf("passage par traitantAbsenceLecteur\n");
  exit(1);
  }

main()
  {
  int pfd[2];
  char tampon[BUFSIZ+1];
  
  if (pipe(pfd) == -1) erreur("pipe");
  
  switch (fork()) {
     case -1 : erreur("fork");
     case 0  :                        	/* fils */                     
               close(pfd[1]);
               printf("fils : attente RC clavier");
	       getchar();
               read(pfd[0],tampon,1);  	/* debloque le fils */
               break;
     default :                        	/* pere */
               signal(SIGPIPE,traitantAbsenceLecteur);
               close(pfd[0]);
               write(pfd[1],tampon,BUFSIZ+1);  /* bloquant car tube plein */
               printf("pere : %d car. ecrits dans tube\n",BUFSIZ+1);
               wait(NULL);
               write(pfd[1],"A",1);  	/* declenche SIGPIPE */
               break;
      }
  }
               
               
