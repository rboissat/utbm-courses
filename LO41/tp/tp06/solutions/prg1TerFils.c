/*-----------------------------------------------------------
    ecrit quelque part.......
    le descripteur est suppose ouvert et son numero
    est fourni en parametre
-------------------------------------------------------------*/
   
#include <stdio.h>

#define LGMAX 100

main(int argc,char* argv[])
  {
  char tampon[LGMAX];
  int d;
  if (argc-1 != 1) {
    fprintf(stderr,"Appel : %s <num desc>\n",argv[0]);
    exit(1);
  }
  d=atoi(argv[1]);
  strcpy(tampon,"salut a toi");
  write(d,tampon,strlen(tampon)+1);
  }
  
