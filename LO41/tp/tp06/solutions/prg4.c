/*-----------------------------------------------------------
               Mise en place de ps | wc -l
-------------------------------------------------------------*/

#include <stdio.h>
#include <errno.h>
#include <unistd.h>

void erreurFin(const char* msg)      {perror(msg);exit(1);}

    
main()
  {
  int pfd[2];

  printf("lancement de la ligne de commande ps | wc -l\n");  
  if (pipe(pfd) == -1) erreurFin("erreur sur pipe");
  
  if (fork()==0) {                                 
               close(pfd[0]);
               dup2(pfd[1],1);     /* redirection sortie */
	       close(pfd[1]);      /* facultatif */
               execl("/usr/bin/ps","ps",NULL);
	       erreurFin("pb sur exec ps");
	       }
  if (fork()==0) {                                 
               close(pfd[1]);
               dup2(pfd[0],0);	  /* redirection entree */
	       close(pfd[0]);    /* facultatif */
               execl("/usr/bin/wc","wc","-l",NULL);
	       erreurFin("pb sur exec wc");
	       }
  close(pfd[0]);                 /* facultatif */    
  close(pfd[1]);                 /* obligatoire pour que ps se termine */
  wait(NULL);
  wait(NULL);
  printf("verifier sur la ligne de commande '/usr/bin/ps | wc -l'\n");
  printf("qu'il y a un processus de moins (ce pseudo-shell)\n"); 
  }
               
               
