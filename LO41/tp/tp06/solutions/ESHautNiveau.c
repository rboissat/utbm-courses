/*---------------------------------------------------------
     Necessite  d'utiliser les E/S bas niveau sur les tubes
-----------------------------------------------------------*/



#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>

int tailleTube(int d)
  {
  struct stat copieInode;
  fstat(d,&copieInode);
  return (int)copieInode.st_size;
  }
  
main()
  {
  int tube[2];
  FILE *f;
  char t[11];
  char *p;
  
  pipe(tube);
  f=fdopen(tube[0],"r");
  
  printf("taille du tube : %d\n",tailleTube(tube[0]));

  printf("ecriture de 10 caracteres\n");
  write(tube[1],"0123456789",10);
  printf("taille du tube : %d\n",tailleTube(tube[0]));

  printf("lecture de 2 caracteres avec read\n");
  read(tube[0],t,2);
  printf("taille du tube : %d\n",tailleTube(tube[0]));

  printf("lecture de 2 caracteres avec fread\n");
  fread(t,1,2,f);  /* => famine pour les autres processus sur read */
  printf("taille du tube : %d\n",tailleTube(tube[0]));

  }
