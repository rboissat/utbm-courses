/*----------------------------------------------------------

           Le probleme des philosophes :
             semaphores et memoire partagee

---------------------------------------------------------*/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include "semaphore.h"

typedef enum {faux,vrai} bool;

#define NB            5
#define TPS_MAX_REPAS 5
#define mutex         0

int     shmid ;      	/* connu de tous les processus */
static  void *adr;  	/* utilise uniquement par le pere*/

static void erreurFin(const char* msg)
  {
  perror(msg);
  exit(1);
  }
  
void initRand()     	/* init generateur nb aleatoires */
  {
  srand(getpid());       
  }

/*---------------- CODE PHILOSOPHE -----------------------*/

void philosophe(int i)
  {
  bool *baguetteLibre, ok = faux;
  void  *adr;

  if ((adr = shmat(shmid,NULL,0)) == (void*) -1) 
     perror("shmat 1"),exit(1);
  baguetteLibre = (bool*) adr;         /* void [i] et [(i+1)%NB] */
  initRand();
  sleep(rand()%3);                     /* pour demarrage aleatoire */
  do {
     P(mutex);
     if (baguetteLibre[i] && baguetteLibre[(i+1)%NB]) {
        baguetteLibre[i] = baguetteLibre[(i+1)%NB] = faux;
        ok=vrai;
        }
     V(mutex);
     }
  while (!ok);
  printf("le philosophe %d commence a manger\n",i);
  sleep(rand()%TPS_MAX_REPAS);
  printf("le philosophe %d a fini de manger\n",i);
  P(mutex);
  baguetteLibre[i] = baguetteLibre[(i+1)%NB] = vrai;
  V(mutex);
  }
  
/*------------- CODE GESTION PROCESSUS -----------------------*/

/* fonction permettant de creer nbFils qui executent la meme fonction*/

void forkn(int nbFils, void (*pauBoulot) ())
  {
  int i;
  for (i=0;i<nbFils;i++) 
     if (fork()==0) {
        (*pauBoulot) (i);
        exit(0);         	/* assure terminaison du fils */
        }
  }

void attenteFin()
  {
  int nbFilstermines=0;
  while (nbFilstermines<NB) {
    wait(NULL);
    nbFilstermines++;
    }
  }

/*------------- CODE GESTION MEMOIRE -----------------------*/

void initMemPartagee(const char* argv0,int taille)
  {
  bool *baguetteLibre;
  int  i;
  key_t cle;
  
  if ((cle = ftok(argv0,'0')) == -1)
     erreurFin("Pb ftok");
  
  if ((shmid = shmget(cle,taille,IPC_CREAT  | IPC_EXCL | 0600)) == -1) 
     erreurFin("Pb shmget 1");
  
  if ((adr = shmat(shmid,NULL,0)) == (void*) -1) 
     perror("shmat 2"),exit(1);

  baguetteLibre = (bool*) adr;         	/* void [i] et [(i+1)%NB] */
  for (i=0;i<NB;i++) baguetteLibre[i]=vrai;
  }
 
void libereMemPartagee()
  {
  shmdt(adr);                      	/* detachement obligatoire */
  if (shmctl(shmid,IPC_RMID,NULL))
     erreurFin("libereMemPartagee");
  }
  
    
/* ---------------------- MAIN ------------------------------*/

main(int argc,char* argv[])
  {
  int cptInitSem = 1;       
  initSem(1,argv[0],&cptInitSem);
  initMemPartagee(argv[0],NB*sizeof(bool));
  forkn(NB,philosophe);
  attenteFin();
  libereSem();
  libereMemPartagee();
  printf("-- TOUS LES PHILOSOPHES ONT MANGE --\n");
  }

