/*--------- memoire.c --------------*/

#include <stdio.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/shm.h>


static int shmid ;
static void *adr;


static void erreurFin(const char* msg)
  {
  perror(msg);
  exit(1);
  }


int initShm(const char* argv0,int taille)
  {
  key_t cle;
  
  if ((cle = ftok(argv0,'0')) == -1)
     erreurFin("Pb ftok");
  
  if ((shmid = shmget(cle,taille,IPC_CREAT  | IPC_EXCL | 0600)) == -1) 
     erreurFin("Pb shmget 1");
     return shmid;
  }

void attacheShm(void *adr, int shmid) {
    if ((adr = shmat(shmid,NULL,0)) == (void*) -1) 
     perror("shmat 2"),exit(1);
}
 
void detacheShm() {
  shmdt(adr);   
}

void libereShm()
  {
  shmdt(adr);                      	/* detachement obligatoire */
  if (shmctl(shmid,IPC_RMID,NULL))
     erreurFin("libereMemPartagee");
  }

