/* memoire.h */
extern int initShm(const char* nomFichier,int taille);
extern void libereShm();
extern void attacheShm(void *adr, int shmid);
extern void detacheShm();
