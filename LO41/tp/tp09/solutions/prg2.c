/****************************************************/
/* prg2.c : incrementation synchronisee             */
/*          solution avec semaphore du prg1.c       */
/* Rq :     le temps de traitement est bien entendu */
/*          plus long, puisqu'il y a 20000000       */
/*          operations P() et V() !                 */
/****************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <sys/wait.h>

pid_t pid;
int shmid, i, j;
int *p;
int semid;
struct sembuf op;
//union semun {int val; } valeur;

P() { op.sem_num = 0; op.sem_op = -1; semop(semid, &op, 1); }
V() { op.sem_num = 0; op.sem_op =  1; semop(semid, &op, 1); }


void erreurSystemeFin(const char* msg,int valeur_retour)
  {
  perror(msg);
  exit(valeur_retour);
  }

main()
{
  shmid = shmget(IPC_PRIVATE, sizeof(int), 0666);
  semid = semget(IPC_PRIVATE, 1, IPC_CREAT | 0666);
  
  p = (int *)shmat(shmid, NULL, 0);
  *p = 0;	//initialisation � 0 du compteur
  V();		//initialisation du semaphore � 1
  switch (pid=fork()) {
     case (pid_t) -1 : erreurSystemeFin("",1);
     case (pid_t)  0 :   //processus fils
	for (i = 0; i < 1000000; i++) {
		P(); (*p)++; V();
	        //printf("Valeur *p dans fils : %d\n", *p);
	}
        exit(0);
     default:		//processus pere
	for (i = 0; i < 1000000; i++) {
		P(); (*p)++; V();
		//printf("Valeur *p dans pere: %d\n", *p);
	}
     }
     
  wait(NULL);
  printf("Valeur finale de l'entier : %d\n", *p);
  shmctl(shmid, IPC_RMID, NULL);
  semctl(semid, 0, IPC_RMID);
  exit(0);

}

