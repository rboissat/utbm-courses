/*-----------------------------------------------------------
                 Gestion du Ctrl-C, Ctrl-Z
-------------------------------------------------------------*/

#include <signal.h>
#include <stdio.h>

void erreur(const char* msg)
  {
  fprintf(stderr,"%s\n",msg);
  exit(1);
  }

void traitantSIGINT(int num)
  {
  if (num!=SIGINT) erreur("y-a un lezard...");
  printf("\n---- Ctrl-C interdit -----\n");
  }
  
void traitantSIGTSTP(int num)
  {
  if (num!=SIGTSTP) erreur("y-a un lezard...");
  printf("\n---- suspension du processus ----\n");
  raise(SIGSTOP);
  }
  
main(int argc,char* argv[])
  {
  int s,i;
  struct sigaction action;
  
  if (argc-1 != 1) {
     fprintf(stderr,"Appel : %s <nb de secondes>\n",argv[1]);
     exit(1);
     }
  s=atoi(argv[1]);

  /*---------------installation des traitant de signaux  */

  /* Avec sigaction :
                    sigemptyset(&action.sa_mask);
                    action.sa_handler=traitantSIGINT;
                    sigaction(SIGINT,&action,NULL);
                    action.sa_handler=traitantSIGTSTP;
                    sigaction(SIGTSTP,&action,NULL);
     Avec signal
  */
  signal(SIGINT,traitantSIGINT);
  signal(SIGTSTP,traitantSIGTSTP);
  
  /*--------------le programme fait son boulot  -> il dort ...  */

  for (i=1;i<=s;i++) {
    sleep(1);
    printf("\r %d secondes ecoules...",i);
    fflush(stdout);               /* force affichage */
    }
  printf("\n");
  }
  
