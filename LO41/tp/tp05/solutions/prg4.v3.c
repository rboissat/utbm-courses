/*-----------------------------------------------------------
                Perte de signaux 
                
   Solution : on masque les signaux tant qu'on n'attend pas
   => l'installation d'un nouveau masque et la mise en attente 
      doivent etre atomiques
                
-------------------------------------------------------------*/

#include <signal.h>
#include <stdio.h>

/* un seul sert dans chaque processus, il faudrait sinon creer */
/* un autre fichier pour le fils et faire un exec              */

const int NBaEnvoyer = 100;

int nbEnvoye=0, nbRecu=0;  
int pidfils;
                   
void traitantPere(int num)
  {
  nbRecu++;
  printf("Nb recus : %d\n",nbRecu);
  kill(pidfils,SIGUSR1);          /* accuse de reception */
  }

void traitantFils(int num) {}
  
main(int argc,char* argv[])
  {
  int res;
  sigset_t ens;
  struct sigaction action;
  
  /*           masquage avant installation traitants   */
   
  sigfillset(&ens);
  sigprocmask(SIG_SETMASK,&ens,NULL);
  sigemptyset(&ens);
  
  /*           le programme fait son boulot             */

  res=fork();
  if (res>0) {                 /* pere */
     /* installation du traitant de SIGUSR1 pour le pere */
     sigemptyset(&action.sa_mask);
     action.sa_handler=traitantPere;
     sigaction(SIGUSR1,&action,NULL);
     /*signal(SIGUSR1,traitantPere);*/
     pidfils=res;
     sigprocmask(SIG_SETMASK,&ens,NULL); /* on peut recevoir les IT */
     while (wait(NULL)==-1);                
     printf("-- fin du pere : %d signaux recus\n",nbRecu);
     }
  else if (res==0) {                            /* fils */
     /* installation du traitant de signal SIGUSR1 pour le fils */
     sigemptyset(&action.sa_mask);
     action.sa_handler=traitantFils;
     sigaction(SIGUSR1,&action,NULL);
     /*signal(SIGUSR1,traitantFils); */
     sigaddset(&ens,SIGUSR1);
     sigprocmask(SIG_SETMASK,&ens,NULL);  /* SIGUSR1 masque par defaut */
     sigemptyset(&ens);                   /* pour sigsuspend           */
     while (nbEnvoye < NBaEnvoyer)  {
        int i;
        nbEnvoye++;
        kill(getppid(),SIGUSR1);   
        for (i=0;i<100000;i++) ;   /* pour mettre en evidence */ 
                                   /* risque interblocage     */  
        sigsuspend(&ens);          /* autorise tout, dont SIGUSR1 */
        printf("Nb envoyes : %d\n",nbEnvoye);
        }       
     printf("-- fin du fils : %d signaux envoyes\n",nbEnvoye);
     }
  
  }
  
