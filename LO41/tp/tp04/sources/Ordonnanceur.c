/* Ordonnanceur */
/* Attention le contenu de cet exercice est incomplet */
/* Ajouter les infos necessaires pour un comportement satisfaisant */

/* Compilation : utiliser option -lrt    */

#include <sched.h> /* obligatoire */
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/resource.h>
#include <sys/ipc.h>

/* Creation du fils */
void proc_fils (int pid) {
int i;
   	 printf(" le processus %d commence a s'executer \n",pid);
    	usleep(100);
    	
	for (i=0 ; i<10000 ; i++) {
		if ((i==4000) || (i==9000) ) {  
			printf(" le processus %d vient de liberer le processeur\n ", pid) ;
			/*  Liberer le  processeur sans blocage */
			
		}
}
}

/* Politique Ordonnancement */
void AfficheOrdonnancement(int pid){
}
/* Modification de la politique Ordonnancement */
void ModifiePolitique(int pid, int prio) {
}

// M A I N
int main (void){
int pidf1, pidf2;


if ((pidf1=fork())==0) { // processus fils 1
   	proc_fils(getpid());
    } 
    
if ((pidf2=fork())==0) { // processus fils 2
   	proc_fils(getpid());
    } 
 
if (pidf1==-1 || pidf2==-1) { printf("erreur\n"); exit(1);    

}