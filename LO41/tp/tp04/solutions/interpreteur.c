/* 		Utililisation de la primitive EXECL		*/
/*           pour simuler un interpreteur de commandes 		*/
/*           -----------------------------------------		*/
/*   Suite a l'acquisition de la ligne de commande a la suite 	*/
/*   de l'invite "> ", l'interpreteur sous-traite l'analyse de	*/
/*   la ligne de commande au Bourne shell (appel de EXECL))	*/ 


#include <stdio.h>

main()
{
   int pid, n, status;
   char s[256];

   write(1,"> ",2);
   while((n=read(0,s,sizeof(s))) != 0)
   {
	s[n-1]='\0';	/* fin de chaine */

	if((pid=fork())==0)
	{		/* c'est le fils */
	   execl("/bin/sh","sh","-c",s,0);
	   exit(1);
	}

	while(wait(&status) != pid);

	/* en cas d'echec de la commande */
	if(status != 0) fprintf(stderr, "Erreur !!\n");

	write(1,"> ",2);
   }
}
