/*---------------------------------------------------------
    Gestion de processus - environnement
    
    1) Ce programme permet d'obtenir des informations
       sur l'environnement d'un processus p�re et d'un fils.
.
----------------------------------------------------------*/

#include <unistd.h>
#include <stdio.h>

void erreurSystemeFin(const char* msg,int valeur_retour)
  {
  perror(msg);
  exit(valeur_retour);
  }

main(int argc,char* argv[],char* arge[])
  {
  pid_t pid;
  
  switch (pid=fork()) {
     case (pid_t) -1 : erreurSystemeFin("",1);   /* break inutile */
     case (pid_t)  0 : {  /* Processus fils */
	execl("./environFils","environFils",NULL);
        exit(2);
	}                          /* idem */
     default: { /* Processus p�re */
	int pidFils,terminaison;
	char **r;
  	char *s;
  	r = arge;
	/* Identification du processus p�re */
        printf("Processus pere=%d\n",getpid());
	printf("*******VARIABLES D'ENVIRONNEMENT DU PERE*******\n");	
	while(s = *r++) printf("%s\n",s);
	printf("*******FIN VARIABLES D'ENVIRONNEMENT DU PERE*******\n\n");
	pidFils=wait(&terminaison);
        printf("Processus fils=%d terminaison=%d\n", pidFils,terminaison);	
        printf("fin du processus pere\n");
        exit(0);
	}
     }
  }
   
        
