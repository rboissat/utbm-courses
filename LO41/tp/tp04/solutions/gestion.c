/*---------------------------------------------------------
    Gestion de processus - caract�ristiques
    
    1) Ce programme permet d'obtenir des informations
       sur l'identit� d'un processus p�re et d'un fils.
    2) La commande Setgid, permet de changer le groupe 
       du fils, mais d�pend des Unix.
    3) Le processus p�re affiche la valeur de terminaison
       du fils.
----------------------------------------------------------*/

#include <unistd.h>
#include <stdio.h>

void erreurSystemeFin(const char* msg,int valeur_retour)
  {
  perror(msg);
  exit(valeur_retour);
  }

main(int argc,char* argv[],char* arge[])
  {
  pid_t pid;

  switch (pid=fork()) {
     case (pid_t) -1 : erreurSystemeFin("",1);   /* break inutile */
     case (pid_t)  0 : {  /* Processus fils */
        int res;
 	/* Identification du processus fils */
        printf("Processus fils=%d de pere=%d de propri�taire=%d et de groupe=%d \n",
	        getpid(),getppid(),getuid(),getgid());
	sleep(1);
	res=setgid(270);
	printf("setgid �choue (=%d), car autoris�e que pour root\n",res);
	/* Nouvelle identification du processus fils */
        printf("Processus fils=%d de pere=%d de propri�taire=%d et de groupe=%d \n",
	        getpid(),getppid(),getuid(),getgid());
	/* sleep 20 pour donner le temps de le d�truire, � partir d'un terminal*/
	sleep(20);
        printf("fin du processus fils\n");
        exit(0);
	}                          /* idem */
     default: { /* Processus p�re */
	int pidFils,terminaison;
	/* Identification du processus p�re */
        printf("Processus pere=%d de pere=%d de propri�taire=%d et de groupe=%d \n",
	        getpid(),getppid(),getuid(),getgid());
	pidFils=wait(&terminaison);
        printf("Processus fils=%d terminaison=%d\n", pidFils,terminaison);	
        printf("fin du processus pere\n");
        exit(0);
	}
     }
  }
   
        
