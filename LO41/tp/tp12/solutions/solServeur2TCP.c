/*****************************************************************************/
/* serveur2TCP :                                                             */
/*                                                                           */
/*   Role      : serveur de terminal                                         */
/*   Appel     : argv[0] port                                                */
/*   Exemple   : argv[0] 2222                                                */
/*   Compilation : cc -lsocket -lnsl serveur2TCP.c commun.c -o serveur2TCP   */
/*****************************************************************************/   

#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/stropts.h>
#include <signal.h>

#include "commun.h"

const int NbMaxAttenteConnexion = 4;	/* doit etre < SOMAXCONN = 5 */


void traitant(int num) {
   printf("-> reception du signal de numero %d\n",num);
   signal(num, SIG_DFL);		/* reinstalle le comportement standard */
   raise(num);
}


void initStructAd(struct sockaddr_in *p, char * port) {
   struct hostent *h;				/* hote */
   struct servent *s;				/* service */
   
   bzero((char *) p , sizeof(*p));		/* tous les champs a 0 */
   p->sin_family = 0;				/* pas besoin d'indiquer adresse locale */
   p->sin_port = htons(atoi(port));		/* port */
}


int creeSocketDeConnexion(int argc, char * argv[]) {
   /* s   : socket permettant la connexion des clients */
   /* ad  : adresse complete = ad IP + port            */ 
   int s;
   struct sockaddr_in ad;
   struct sockaddr *pad = (struct sockaddr *) &ad;
   
   /*--------- creation socket ---------*/
   s = socket(AF_INET, SOCK_STREAM, 0);		/* 0 inutile dans ce cas */
   if (s < 0) {
   	perror("serveur : socket"); exit(1);
   }
   
   /*--------- affectation au port identifiant le serveur ---------*/
  
   initStructAd(&ad, argv[1]);
   if (bind(s, pad, sizeof(ad)) < 0) {
   	perror("serveur : bind"); exit(1);
   }
   

   /*--------- indication du nombre max de connections en attente ---------*/
   listen(s, NbMaxAttenteConnexion);
   
   return s;
}


int attenteConnexionSur(int sDeConnexion) {
   /* sDeConnexion : socket permettant la connexion des clients  */
   /* sExtremite   : socket extremite cree a chaque connexion    */
   /* ad           : adresse complete client = ad IP + port      */
   int sExtremite, lgad;
   struct sockaddr_in ad;
   struct sockaddr *pad = (struct sockaddr *) &ad;

   lgad = sizeof(ad);
   do
   	sExtremite = accept(sDeConnexion, pad, &lgad);	/*a NULL si indifferent */
   while (sExtremite == -1);	/* accept peut echouer a la reception d'un signal */
   return sExtremite;
}


void gereServiceAvec(int socket, char *prg) {
   if (dup2(socket, 0) == -1) {
   	perror("serveur : GereServiceAvec 0"); exit(1);
   }
   if (dup2(socket, 1) == -1) {
   	perror("serveur : GereServiceAvec 1"); exit(1);
   }
      if (dup2(socket, 2) == -1) {
   	perror("serveur : GereServiceAvec 2"); exit(1);
   }
   close(socket);
   if (execl(prg, prg, NULL) == -1) {
   	perror("serveur : gereServiceAvec 3"); exit(1);
   }
}


void creeFilsPourGererConnexion(int socket, int argc, char ** argv) {
   switch (fork()) {
      case -1 : perror("serveur : creeFilsPourGererConnexion 1");
      		exit(1);
      case  0 : if (argc -1 == 1) gereTerminalAvec(socket);
      		else if (argc - 1 == 2) gereServiceAvec(socket, argv[2]);
		break;
      default : close(socket);	/* important pour rupture connexion */
   }
}


void verifieParam(int argc, char *argv[]) {
   if (argc-1<1) {
   	fprintf(stderr, "%s port [service]\n", argv[0]);
	exit(1);
   } else {
   	if (! chaineEstNumerique(argv[1])) {
	   fprintf(stderr, "port doit etre une valeur numerique !\n");
	   exit(1);
	}
	if (argc-1 == 1)
	   fprintf(stderr, "service non mentionne -> acces au terminal %s\n", ttyname(0));
	else
	   if (access(argv[2], X_OK) == -1) {
	   	fprintf(stderr, "%s : service inexistant dans le repertoire courant\n", argv[2]);
		exit(1);
	   }
   }
}

   
   
int main(int argc, char* argv[])
{
   int socketConnexion, i;

   verifieParam(argc, argv);
   socketConnexion = creeSocketDeConnexion(argc, argv);
   printf("socket de Connexion : %d\n", socketConnexion);
   for (;;) {
   	int socketCommunication;
	socketCommunication = attenteConnexionSur(socketConnexion);

   	creeFilsPourGererConnexion(socketCommunication, argc, argv);
   }
   
}

   
   
 
