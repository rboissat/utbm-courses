/* serveurUDP.c */

#include <stdio.h>
#include <unistd.h>
#include <stropts.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <pwd.h>
#include <fcntl.h>

#define LGUSER  20
#define LGREP  256
#define PORT  2222


int creersock(int *port, int type) {
   int desc;
   struct sockaddr_in nom;
   int longueur;
   
   /* creation du socket */
   if ((desc = socket (AF_INET, type, 0)) == -1) {
   	fprintf(stderr, "creation impossible !\n");
	exit(2);
   }
   
   /* preparation de l'adresse */
   bzero((char *)&nom, sizeof(nom));
   nom.sin_port = *port;
   nom.sin_addr.s_addr = INADDR_ANY;
   nom.sin_family = AF_INET;
   
   /* attachement du socket cree � l'adresse locale */
   if (bind(desc, (struct sockaddr*)&nom, sizeof(nom))) {
   	fprintf(stderr, "nommage de socket impossible\n");
	exit(3);
   }
   longueur = sizeof(nom);
   
   /* recuperation de l'adresse locale si inconnue (no port non specifie) */
   if (getsockname(desc, (struct sockaddr*)&nom, &longueur)) {
   	fprintf(stderr, "pb obtention du nom de socket !\n");
	exit(4);
   }
   
   /* mise au format reseau du numero de port */
   *port = ntohs(nom.sin_port);
   return desc;
}


int main(int argc, char * argv[])
{
   int lg, d, n;
   int sock, port;
   char user[LGUSER];
   struct sockaddr_in adr;
   struct passwd * getpwdnam(), *p;
   struct reponse {
   	char type;
	char info[LGREP];
        } rep;

   /* creation du socket de communication avec liaison au port UDP 2222 */
   port = PORT;
   if ((sock = creersock(&port, SOCK_DGRAM)) == -1) {
   	fprintf(stderr, "echec creation de socket !\n");
	exit(2);
   } else { fprintf(stdout, "numero de socket cree : %d\n\n",sock); }
   
   /* detachement du terminal */
   close(0);
   /*close(1);  si on desire l'affichage des messages*/
   close(2);
   if ((d = open("/dev/tty", O_RDWR,0)) > -1) {
   	/* if ((ioctl(d, TIOCNOTTY, 0)) == -1) { exit(1); } */
	close(d);
   }
   
   /* creation d'une session de travail */
   /* setgrp(); */
   
   /* attente requete sur le socket */
   while (1) {
     	lg = sizeof(adr);
	/* initialisation a zero de la zone de reception */
	bzero(user, LGUSER);
	/* initialisation a zero de la zone de reponse */
	bzero((char *)&rep, sizeof(rep));
	
	fprintf(stdout, "en attente de client...\n");
	n = recvfrom(sock,user,LGUSER,0,(struct sockaddr*)&adr,&lg);
	
	/* traitement specifique du serveur */
	if ((p = getpwnam(user)) == NULL) {
	   rep.type = '1';
	} else {
	   rep.type = '2';
	   sprintf(rep.info, "%d %d %s %s %s\n", p->pw_uid,
	   p->pw_gid, p->pw_gecos, p->pw_dir, p->pw_shell);
	}
	fprintf(stdout, "\trequete du client %s\n", user);
	
	/* emission de la reponse */
	n = sendto(sock,(char *)&rep,sizeof(struct reponse),0,(struct sockaddr*)&adr,lg);
   	fprintf(stdout, "\treponse au client %s\n", user);
   }
}
	   
