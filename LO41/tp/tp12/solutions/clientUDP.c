/*****************************************************************************/
/* clientUDP.c :                                                             */
/*                                                                           */
/* Programme permettant d'obtenir les informations contenues dans le         */
/* fichier /etc/passwd d'une machine de nom donne en premier parametre       */
/* concernant un utilisateur dont le nom est donne en second parametre       */
/* Le service correspondant est associe au port 2222 sur la machine distante */
/*                                                                           */
/* compilation : cc -lsocket -lnsl clientUDP.c -o clientUDP                  */
/* appel       : client sunserv fougeres                                     */
/*****************************************************************************/


#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>

#define PORT  2222
#define LGREP  256

int main(int argc, char * argv[])
{
   int sock, uid, gid;
   int lg;
   char p[LGREP];
   
   /* reponse du serveur */
   struct {
 	 char type;
	 char info[LGREP];
	 } rep;
   struct sockaddr_in adr;
   struct hostent *hp;
   
   if (argc < 3) {
   	fprintf(stderr, "nombre de parametres incorrect!\n");
        exit(1);
   }
   
   /* preparation de l'adresse du serveur */
   if ((hp = gethostbyname(argv[1])) == NULL) {
   	fprintf(stderr, "nom de machine incorrect!\n");
	exit(1);
   }
   bzero((char *)&adr, sizeof(adr));
   sock = socket(AF_INET, SOCK_DGRAM, 0);
   adr.sin_family = AF_INET;
   adr.sin_port = htons(PORT);
   
   bcopy(hp->h_addr, &adr.sin_addr, hp->h_length); /*maj de l'adresse*/

   /* envoi du message : nom de l'utilisateur */
   if (sendto(sock,argv[2],strlen(argv[2]),0,(struct sockaddr *)&adr,sizeof(adr)) == -1) {
   	fprintf(stderr, "pb sendto!\n");
	exit(1);
   }
   
   /* attente de la reponse du serveur */
   lg = sizeof(adr);
   recvfrom(sock,(char *)&rep,sizeof(rep),0,(struct sockaddr*)&adr,&lg);
   
   /* exploitation de la reponse */
   if (rep.type == '1') {
   	fprintf(stderr, "%s : utilisateur inconnu sur %s!\n", argv[2], argv[1]);
   } else {
   	printf("utilisateur %s sur %s\n\n", argv[2], argv[1]);
	sscanf(rep.info, "%d %d %[\n]", &uid, &gid, p);
	printf("\tuid = %d ; gid = %d\n", uid, gid);
	printf("\t%s\n",p);
   }
}

/***** EXECUTION :
{fougeres/sunserv2}45: serveurUDP
numero de socket cree : 3

en attente de client...
        requete du client fougeres
        reponse au client fougeres
en attente de client...
        requete du client canalda
        reponse au client canalda
en attente de client...
        requete du client xxxx
        reponse au client xxxx
en attente de client...

{fougeres/sunus5_gi_07}46: clientUDP sunserv2 fougeres
utilisateur fougeres sur sunserv2

        uid = 2520 ; gid = 302

{fougeres/sunus5_gi_07}47: clientUDP  sunserv2 canalda
utilisateur creput sur sunserv2

        uid = 2767 ; gid = 302

{fougeres/sunus5_gi_07}48: clientUDP sunserv2 xxxx
xxxx : utilisateur inconnu sur sunserv2!
*****/


   	
