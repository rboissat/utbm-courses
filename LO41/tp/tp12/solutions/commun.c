#include <stdio.h>
#include <ctype.h>
#include <sys/types.h>

#include "commun.h"

bool chaineEstNumerique(char *p) {
   bool ok = vrai;
   while(*p != '\0' && ok) {
   	ok = isdigit(*p);
	p++;
   }
   return ok;
}


void gereTerminalAvec(int socket) {
   for(;;) {
   	fd_set ens;
	FD_ZERO(&ens);
        FD_SET(0,&ens);
        FD_SET(socket,&ens);
    	select(1+socket,&ens,NULL,NULL,NULL);
    	if (FD_ISSET(0,&ens)) {
	   char c;
	   read(0, &c, 1);
	   write(socket, &c, 1);
       	} else {
	   if (FD_ISSET(socket,&ens)) {
	   	char ligne[TLIGNE];
	   	int nbCar;
	   	nbCar = read(socket, ligne, TLIGNE);
	   	if (nbCar == 0) exit(0);
	   	write(1, ligne, nbCar);
	   }
	}
   }
}
