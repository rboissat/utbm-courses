/*****************************************************************************/
/* client2TCP.c                                                              */
/*                                                                           */
/* Role        : client "universel d'un serveur tcp/ip                       */
/* Appel       : argv[0] machine service|port                                */
/* Exemple     : client2TCP sunserv ftp                                      */
/*               client2TCP sunserv2 21                                      */
/* Compilation : cc -lsocket -lnsl client2TCP.c commun.c -o client2TCP       */
/*****************************************************************************/


#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <signal.h>

#include "commun.h"


void traitant(int num) {
   printf("-> reception du signal de numero %d\n",num);
   signal(num, SIG_DFL);	/* reinstalle comportement standard */
   raise(num);
}


void initStructAdServeur(struct sockaddr_in *p, int argc, char * argv[]) {
   struct hostent *h;		/* hote */
   struct servent *s;		/* service */
   
   /*--------- recuperation adresse IP ---------*/

		/*========== (5) ==========*/


   /*--------- recuperation numero de PORT ---------*/
   if (chaineEstNumerique(argv[2])) {
     	s = (struct servent*) malloc(sizeof(struct servent));
	s->s_port = htons(atoi(argv[2]));	/* num port normalise 2 octets */
						/* consecutifs */
   } else {	/* sinon on se base sur le num de port du client */
   	s = getservbyname(argv[2], "tcp");
   	if (s == NULL) {
   	   fprintf(stderr, "%s est non disponible\n", argv[2]); exit(1);
   	}
   }
   
   /*--------- initialisation de la structure ---------*/

		/*========== (6) ==========*/

}


int etablissementConnexion(int argc, char * argv[]) {
   /* s         : socket extremite d'une connexion */
   /* adServeur : ad IP + port du socket distant   */ 
   int s;					/* descripteur de socket */
   struct sockaddr_in adServeur;		/* in pour domaine Internet */
   
   if (argc < 3) {
   	fprintf(stderr, "client : %s machine service|port \n", argv[0]);
	exit(0);
   }
   
   /*--------- creation de la socket ---------*/
   
		/*========== (7) ==========*/


   initStructAdServeur(&adServeur, argc, argv);
   
   /*--------- connexion ---------*/
   
		/*========== (8) ==========*/
		
   return s;
}


int main(int argc, char* argv[])
{
   int socket, i;

   socket = etablissementConnexion(argc, argv);
   gereTerminalAvec(socket);			/*definie dans commun.c */
}
