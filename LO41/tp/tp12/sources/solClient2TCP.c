/*****************************************************************************/
/* client2TCP  :                                                             */
/*                                                                           */
/* Role        : client "universel d'un serveur tcp/ip                       */
/* Appel       : argv[0] machine service|port                                */
/* Exemple     : client2TCP sunserv ftp                                      */
/*               client2TCP sunserv2 21                                      */
/* Compilation : cc -lsocket -lnsl client2TCP.c commun.c -o client2TCP       */
/*****************************************************************************/


#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <signal.h>

#include "commun.h"

void traitant(int num) {
   printf("-> reception du signal de numero %d\n",num);
   signal(num, SIG_DFL);	/* reinstalle le comportement standard */
   raise(num);
}


void initStructAdServeur(struct sockaddr_in *p, int argc, char * argv[]) {
   struct hostent *h;		/* hote */
   struct servent *s;		/* service */
   
   /*--------- recuperation adresse IP ---------*/
   h = gethostbyname(argv[1]);
   if (h == NULL) {
   	fprintf(stderr, "%s est une machine inconnue\n", argv[1]); exit(1);
   }
   
   /*--------- recuperation numero de PORT ---------*/
   if (chaineEstNumerique(argv[2])) {
     	s = (struct servent*) malloc(sizeof(struct servent));
	s->s_port = htons(atoi(argv[2]));	/* num port normalise comme 2 octets */
						/* consecutifs */
   } else {	/* sinon on se base sur le num de port du client */
   	s = getservbyname(argv[2], "tcp");
   	if (s == NULL) {
   	   fprintf(stderr, "%s est non disponible\n", argv[2]); exit(1);
   	}
   }
   
   /*--------- initialisation de la structure ---------*/
   bzero((char *) p , sizeof(*p));		/* tous les champs a 0 */
   bcopy(h->h_addr, &p->sin_addr, h->h_length);	/* adresse IP */
   p->sin_family = h->h_addrtype;		/* type d'adresse IP */
   p->sin_port = s->s_port;			/* port */
}


int etablissementConnexion(int argc, char * argv[]) {
   /* s         : socket extremite d'une connexion */
   /* adServeur : ad IP + port du socket distant   */ 
   int s;					/* futur descripteur de socket */
   struct sockaddr_in adServeur;		/* in pour domaine Internet */
   
   if (argc < 3) {
   	fprintf(stderr, "client : %s machine service|port \n", argv[0]);
	exit(0);
   }
   
   s = socket(AF_INET, SOCK_STREAM, 0);
   if (s < 0) {
   	perror("client : socket"); exit(1);
   }
   
   initStructAdServeur(&adServeur, argc, argv);
   
   if (connect(s, (struct sockaddr *) &adServeur, sizeof(adServeur)) < 0) {
   	perror("client : connect"); exit(1);
   }

   return s;
}


int main(int argc, char* argv[])
{
   int socket, i;

   socket = etablissementConnexion(argc, argv);
   gereTerminalAvec(socket);			/*definie dans commun.c */
}


/* execution :
{fougeres}63: hostname
sunserv
{fougeres}64: serveur2TCP 2222
service non mentionne -> acces au terminal /dev/pts/16
essai sur le terminal fourni par le serveur2TCP sur sunserv
message bien recu
deconnexion

{fougeres}553: hostname
sunus5_gi_07
{fougeres}554: client2TCP sunserv 2222
essai sur le terminal fourni par le serveur2TCP sur sunserv
message bien recu
deconnexion    
{fougeres}555: 
*/


   
 
