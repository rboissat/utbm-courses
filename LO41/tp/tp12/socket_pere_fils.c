#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <errno.h>


void main ()
{
  int s, recu, lg;
  struct sockaddr_un adresse;
  char *SocketName="MaSocket", *MsgS="Coucou", *MsgR;

/* Select IP */
  adresse.sun_family=AF_UNIX;

/* Pathnames the socket */
  strcpy(adresse.sun_path,SocketName);

/* Creates the socket and returns the descriptor */
  if ( (s=socket (AF_UNIX,SOCK_DGRAM,0))==-1)
    {
      printf ("Error opening Socket !\n");
      perror("Socket");
    }

/* returns the port number  */
  if ( bind(s, (const struct sockaddr *)&adresse, sizeof(adresse))==-1)
    {
      printf ("Link Error !\n");
      perror("BIND");
    }

  switch (fork())
    {
    case -1 :
      printf("Echec du fork!\n");
      exit (1);
    case 0 :
      if (sendto(s,MsgS,sizeof(MsgS),0,(struct sockaddr*)&adresse,sizeof(adresse)) == -1)
	{
	printf("Error sending message to father !\n");
	perror("sendto");
	}
      printf("Message envoye au pere : %s \n",MsgS);
      close(s);
      exit (0);
    default :
      if (recu=recvfrom(s,MsgR,256,0,(struct sockaddr*)&adresse,&lg) == -1)
	{
	printf("Error receiving message from son!\n");
	perror("recvfrom");
	}
      printf("Message recu du fils : %s \n",MsgR);
      close(s);
      exit (0);
    }
}

