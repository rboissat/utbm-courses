#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

/*
  Commentaires ici
*/

char *filename = "fork_output.txt";

char message1[] = "MSG1: Test de  1\n";
char message2[] = "MSG2: Test de  2\n";

int partie_close = 0;
int partie_fork  = 1;
int partie_wait  = 1;

int main(int argc, char *argv[]) {

    int fd;
    int result; 
    int status;

    /* 0 --> Entrée standard
     * 1 --> Sortie standard
     * 2 --> Sortie erreur */

    if (partie_close == 1) {
        close(1);
    }

    fd = open(filename,O_CREAT|O_RDWR|O_TRUNC,S_IRWXU|S_IRWXG|S_IRWXO);

    if (fd < 0) {
        printf("erreur ouverture fichier\n");
        exit(-1);
    }
    printf("Open OK, returned fd: %d, sizeof(filename) %d sizeof(message2) %d\n",fd,sizeof(filename),sizeof(message2));


    if (partie_fork == 0) { // partie1

        write(fd,message2,sizeof(message2));

    } else {
        
        printf("mon pid: %d avant le fork\n",getpid());
        result = fork();
        if (result == 0) {
            // fils ou parent ? /*réponse: fils*/
            sleep(5);
            printf("resultat == 0, mon pid: %d\n",getpid());
            printf("writing message2, resultat du fork: %d\n",result);
            write(fd,message2,sizeof(message2));
            close(fd);
            exit(0);
        } else {
            // fils ou parent ? /*réponse: parent*/
            printf("resultat != 0, mon pid: %d\n",getpid());
            printf("ecriture du message1, resultat du fork: %d\n",result);
            write(fd,message1,sizeof(message1));
            close(fd);

            if (partie_wait == 1) {
                
                result = wait(&status);
                printf("wait retour %d, status %d\n",result,status);
            }
        }
        exit(0);
    }


}
