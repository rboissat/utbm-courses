/*-----------------------------------------------------------
         Duplication de l'espace d'adressage
-------------------------------------------------------------*/

#include <unistd.h>
#include <stdio.h>

int erreurSystemeFin(const char* msg,int valeur_retour)
  {
  perror(msg);
  return(valeur_retour);
  }

int erreurFin(const char* format,const char* msg)
  {
  fprintf(stderr,format,msg);
  return(1);
  }

int n = 10;
 
main(int argc,char* argv[],char* arge[])
  {
  n++;
  
  switch (fork()) {
     case (pid_t) -1 : erreurSystemeFin("",1); /* break inutile */
     case (pid_t)  0 :   
        sleep(2);       /* pour que l'ordonanceur elise le pere */
        printf("FILS n : adresse = %p  valeur = %d\n",&n,n);
        return(0);                    
     default:           /* processus pere */
        n++;
        printf("PERE n : adresse = %p  valeur = %d\n",&n,n);
	return(0);
     }
  }
   
        
