/*-----------------------------------------------------------
                   Redirection des E/S
               
       Rq : le programme de test est un autre exemple
            montrant que les E/S de haut niveau peuvent etre
            inadaptees dans les cas "limites"
            ici ecriture puis lecture dans un meme fichier
            
            pas de pb avec les E/S bas niveau, 2 entrees dans la
            table des fichiers ouverts -> 2 positions dans le
            fichier
-------------------------------------------------------------*/

#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>

//-------------------------------------------------------------

// en dehors du numero de desc, pas d'indication sur le mode 
// d'ouverture => sortie en erreur

void redirige(int desc,const char* fich)
  {
  int nouvDesc;
  switch (desc) {
     case 0 : nouvDesc=open(fich,O_RDONLY);
              break;
     case 1 : nouvDesc=open(fich,O_CREAT | O_TRUNC | O_WRONLY);
              break;
     default : fprintf(stderr,"je ne peux rediriger correctement le descripteur %d sur %s\n",desc,fich);
               fprintf(stderr,"ne connaissant le mode d'ouverture de %s\n",fich);
               exit(1);
     }
  if (nouvDesc == -1) 
     perror("erreur sur open dans redirige"),exit(1);
  dup2(nouvDesc,desc);
  close(nouvDesc);
  }

//-------------------------------------------------------------

#define TailleChaine 100
#define NomFichier   "bid"

main()
  {
  int d;
  char *texte = "blabla";
  char chaineLue[TailleChaine];
  
  redirige(1,NomFichier);
//  printf("%s\n",texte);              // E/S haut niveau : pb
  write(1,texte,strlen(texte)+1);    // E/S bas niveau  : ok
  
  redirige(0,NomFichier);
//  scanf("%s",chaineLue);       
  read(0,chaineLue,TailleChaine);
  
  if (!strcmp(texte,chaineLue))
     fprintf(stderr,"l'entree et la sortie standard ont bien ete rediriges \n");
  else
     fprintf(stderr,"y a pb \n");
     
  unlink(NomFichier);                  // suppression de NomFichier
  }
