/* coiffeur.c */
#include <stdio.h>
#include <pthread.h>

#define NbTh 10      //Nombre de processus symbolisant les clients
#define N  8	//Nombre de chaises dans le salon de coiffure

pthread_t tid[NbTh+1];
pthread_mutex_t mutex;
pthread_cond_t attendre, dormir;

//initialisations
int NbClientsAttente=0;    //Nombre de clients en attente d'etre coiffes


void Coiffer ()
{
pthread_mutex_lock(&mutex);
        if(NbClientsAttente>0) 
		{
		pthread_cond_signal(&attendre); // dit � un client de s'installer sur le fauteuil
		
		}
	else       {
                         
		printf("le coiffeur dort car pas de clients \n");
                pthread_cond_wait(&dormir, &mutex);
  	    	pthread_cond_signal(&attendre); // dit � un client de s'installer sur le fauteuil
		}
 pthread_mutex_unlock(&mutex);
}

void Client(int i)
{
pthread_mutex_lock(&mutex);
 if(NbClientsAttente<N) 
		{
		NbClientsAttente ++;
		pthread_cond_signal(&dormir); // avertit le coiffeur qu'il est la et s'assoit
		printf("Le client %d avertit le coiffeur qu'il est la et s'assoit \n",i);
		pthread_cond_wait(&attendre,&mutex); // attend qu'on lui dise de s'installer
		NbClientsAttente --;
		printf("Le coiffeur invite le client %d pour s'installer et le coiffer \n",(int)i);
}
else printf("Le client %d ne trouve pas de place\n", i);
pthread_mutex_unlock(&mutex);
}


void * fonc_coiffeur()
{                          

while (1)  {
	Coiffer();
/* temps de coiffure d'un client */
usleep(200000);
   }
}

void * fonc_client(void *i)
{
	Client((int)i);

/* temps de coiffure */
usleep(200000);
printf("Le client %d quitte le salon\n", (int) i);
}


int main()
{
int num;

pthread_mutex_init(&mutex,0);
pthread_cond_init(&attendre,0);
pthread_cond_init(&dormir,0);

// creation de la thread coiffeur
pthread_create(tid+NbTh,0,(void *(*)())fonc_coiffeur,(void*)NbTh);

//creation des threads clients
for(num=0;num<NbTh;num ++)
        pthread_create(tid+num,0,(void *(*)())fonc_client,(void*)num);

//attend la fin de toutes les threads clients
for(num=0;num<NbTh;num ++)
        pthread_join(tid[num],NULL);
       
        
/* liberation des ressources");*/
 pthread_mutex_destroy(&mutex);
 pthread_cond_destroy(&attendre);
 pthread_cond_destroy(&dormir);   

exit(0);
}
