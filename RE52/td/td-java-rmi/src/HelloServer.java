import java.rmi.registry.*;
import java.rmi.server.*;

public class HelloServer implements Hello {

  public HelloServer() {}

  public String sayHello(){
    return "OH HAÏ!";
  }

  public static void main(String args[]) {
    try {
      HelloServer obj = new HelloServer();
      Hello stub = (Hello) UnicastRemoteObject.exportObject(obj, 0);
      int port = 1099;
      LocateRegistry.createRegistry(port);

      Registry registry = LocateRegistry.getRegistry(port);
      registry.bind("Hello", stub);

      System.err.println("Server ready");

    } catch (Exception e) {

      System.err.println("Server exception: " + e.toString());
      e.printStackTrace();
    }
  }
}

